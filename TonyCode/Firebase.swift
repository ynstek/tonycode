//
//  Firebase.swift
//  TonyCode
//
//  Created by Yunus Tek on 22.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import UserNotifications
import UserNotificationsUI

extension AppDelegate : MessagingDelegate, UNUserNotificationCenterDelegate {
    
    // MARK: Firebase
    func createFirebase(_ application: UIApplication) {
        // Override point for customization after application launch.
        
        // register_for_notifications
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound ]
            
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications() // if else ten sonra olmali.
        
        // MARK: Firebase Configure
        FirebaseApp.configure()
        
        // Uygulama acikken bildirim gelmesini saglar
        let current = UNUserNotificationCenter.current()
        current.delegate = self
        
        // Uygulama bildirim sayisini sifirlama
        application.applicationIconBadgeNumber = 0
        current.removeAllDeliveredNotifications() // To remove all delivered notifications
        current.removeAllPendingNotificationRequests() // To remove all pending notifications which are not delivered yet but scheduled.
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self //as? MessagingDelegate
        // [END set_messaging_delegate]
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed. Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // uygulama acikken bildirimi yakalar
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (_ options: UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.badge, .alert, .sound])
    }
    
    // MARK: [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        application.applicationIconBadgeNumber += 1
        print(userInfo)
    }
    
    // MARK: Bildirime tiklaninca
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        let dict = userInfo["aps"] as! NSDictionary
        if let d : [String : Any] = dict["alert"] as? [String : Any] {
            let body : String = d["body"] as! String
            let title : String = d["title"] as! String
            print("Title:\(title)\nbody:\(body)")
        } else {
            let dict = userInfo["aps"] as! NSDictionary;
            print(dict)
            let message = dict["alert"];
            print("%@", message!);
        }
        
        if let sellerid = userInfo["sellerid"] as? String {
            if let id = Int(sellerid) {
                print("SellerID", sellerid)
                if id != -1 {
                    AppDelegate.isNofiticationSeller = true
                    GlobalVariables.shared.selectedSellerId = id
                }
            }
        }
        AppDelegate.isNofiticationBildirim = true
        NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadSeller"), object: nil)
        
        
        application.applicationIconBadgeNumber -= 1
        Messaging.messaging().appDidReceiveMessage(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        GlobalFunctions.shared.firebaseSetTokenAndTopic(fcmToken)
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

