//
//  Extension.swift
//  TonyCode
//
//  Created by Yunus Tek on 8.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation
import UIKit

extension Array {
    // Indexlerin yerini degistir.
    func reArrangeIndex<T>(fromIndex: Int, toIndex: Int) -> Array<T> {
        var arr = self
        let element = arr.remove(at: fromIndex)
        arr.insert(element, at: toIndex)
        return arr as! Array<T>
    }
}

extension UIButton {
    func setBorderColor(_ borderWidth: CGFloat = 1, _ borderColor: CGColor = UIColor.black.cgColor) {
        self.layer.borderColor = borderColor
        self.layer.borderWidth = borderWidth
    }
}

extension UILabel {
    func setBorderColor(_ borderWidth: CGFloat = 1, _ borderColor: CGColor = UIColor.black.cgColor) {
        self.layer.borderColor = borderColor
        self.layer.borderWidth = borderWidth
    }
}

extension UIViewController {
    func backButton() {
        navigationController?.navigationBar.topItem?.title = " " // x10 character
        navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    func dismissKeyBoard() {
        view.endEditing(true)
    }
}

extension UIViewController {
    func presentTransparentNavigationBar() {
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
        self.navigationController!.navigationBar.isTranslucent = true
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.setNavigationBarHidden(false, animated:true)
        self.navigationItem.hidesBackButton = true
        //        self.edgesForExtendedLayout = UIRectEdge.None // Kenarlar
        //        self.navigationController!.navigationBar.barTintColor = GlobalFunctions.shared.getColor("black")
    }
}


extension Double {
    func converToString() -> String {
        // X or X.X
        return String(format: self == floor(self) ? "%.0f" : "%.1f", self)
    }
}

extension Float {
    func converToString() -> String {
        // X or X.X
        return String(format: self == floor(self) ? "%.0f" : "%.1f", self)
    }
}

extension UIImage { // Boyutunu kucult
    func resizeUI(size:CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, true, self.scale)
        self.draw(in: CGRect(origin: CGPoint.zero, size: size))
        
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resizedImage
    }
}

extension UIColor {
    // RGB
    static func colorWithRedValue(_ redValue: CGFloat, greenValue: CGFloat, blueValue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: redValue/255.0, green: greenValue/255.0, blue: blueValue/255.0, alpha: alpha)
    }
    
    // #Hex Color Code
    // UIColor(hexString: "#E52320")
    convenience init(_ hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension Date {
    func ToString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        return formatter.string(from: self)
    }
}

extension String {
    func ToDate() -> Date {
        let formatter = DateFormatter()
        // 2018-04-11 22:14:05
        if self.count <= 10 {
            formatter.dateFormat = "dd/MM/yyyy"
        } else {
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        }
        
        if let thisDate = formatter.date(from: self) {
            return thisDate
        } else {
            return Date()
        }
    }
    
    func replaceTr() -> String {
        let responseData = self.data(using: .windowsCP1254)
        var stext = String(data: responseData!, encoding: .windowsCP1254)!
        
        stext = stext.replacingOccurrences(of: "\\u011f", with: "ğ")
        stext = stext.replacingOccurrences(of: "\\u011e", with: "Ğ")
        stext = stext.replacingOccurrences(of: "\\u0130", with: "İ")
        stext = stext.replacingOccurrences(of: "\\u0131", with: "ı")
        stext = stext.replacingOccurrences(of: "\\u0130", with: "İ")
        stext = stext.replacingOccurrences(of: "\\u00f6", with: "ö")
        stext = stext.replacingOccurrences(of: "\\u00d6", with: "Ö")
        stext = stext.replacingOccurrences(of: "\\u00fc", with: "ü")
        stext = stext.replacingOccurrences(of: "\\u00dc", with: "Ü")
        stext = stext.replacingOccurrences(of: "\\u015f", with: "ş")
        stext = stext.replacingOccurrences(of: "\\u015e", with: "Ş")
        stext = stext.replacingOccurrences(of: "\\u00e7", with: "ç")
        stext = stext.replacingOccurrences(of: "\\u00c7", with: "Ç")
        
        return stext;
    }
    
    func strikethrough() -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        attributedString.addAttribute(NSAttributedStringKey.baselineOffset, value: 0, range: NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.styleThick.rawValue), range: NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedStringKey.strikethroughColor, value: UIColor.gray, range: NSMakeRange(0, attributedString.length))
        
        return attributedString
    }
    
}

extension UIView {
    func setGradientBackground(one: UIColor, two: UIColor) {
        let gl = CAGradientLayer()
        gl.frame = bounds
        gl.colors = [one.cgColor, two.cgColor]

        // yukaridan asagiya
        gl.locations = [0.5, 1.0]
        gl.startPoint = CGPoint(x: 0.5, y: 0.0)
        gl.endPoint = CGPoint(x: 0.5, y: 1.0)
        
        layer.insertSublayer(gl, at: 0)
        
        // 0,0   .5,0   1,0
        // 0,.5  .5,.5  1,.5
        // 0,1   .5,1   1,1
    }
}

extension UIImageView {
    func df(_ url: URL) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String?) {
        // Api urli ekli
        if link != nil {
            guard let url = URL(string: GlobalVariables.shared.url + "/" + link!) else { return }
            df(url)
        }
    }
}

extension UIViewController {
    func activityOpen() {
        // navigationbar Hidden
//            self.navigationController?.isNavigationBarHidden = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        // Open
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ActivityView")
        self.addChildViewController(vc)
        vc.view.frame = self.view.frame
        self.view.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    
        // tabbar Hidden
        var i = 4
        while i >= 0 {
            ((self.tabBarController?.tabBar.items as NSArray?)?[i] as? UITabBarItem)?.isEnabled = false
            i -= 1
        }
        
    }

    func activityClose() {
        DispatchQueue.main.async() {
            // if it is open, close it
            if let cvc = self.childViewControllers.last {
                cvc.view.backgroundColor = UIColor.clear
                cvc.view.isHidden = true
                cvc.view.alpha = 0
                cvc.willMove(toParentViewController: nil)
                cvc.view.removeFromSuperview()
                cvc.removeFromParentViewController()
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                // navigationbar Hidden
                self.navigationController?.isNavigationBarHidden = false

                // tabbar Hidden
                var i = 4
                while i >= 0 {
                    ((self.tabBarController?.tabBar.items as NSArray?)?[i] as? UITabBarItem)?.isEnabled = true
                    i -= 1
                }
            }
        }
    }
}

