//
//  GlobalFunctions.swift
//  Orca POS Mobile
//
//  Created by Yunus TEK on 21.03.2017.
//  Copyright © 2017 Orca Businesss Solutions. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import FirebaseMessaging

private let _sharedGlobalFunctions = GlobalFunctions()
class GlobalFunctions : NSObject, URLSessionDelegate, CLLocationManagerDelegate {
    
    // MARK: - SHARED INSTANCE
    class var shared : GlobalFunctions {
        return _sharedGlobalFunctions
    }
    
    func firebaseSetTokenAndTopic(_ tokenId: String? = nil) {
        // TODO: Firebase - Abone olma uygulama acildiktan sonra yapilmali. yoksa null veriyor ve abone olamiyor.
        // [START subscribe_topic]
        Messaging.messaging().subscribe(toTopic: "news")
        print("Subscribed to news topic")
        // [END subscribe_topic]
        
        // [START log_fcm_reg_token]
        var token = Messaging.messaging().fcmToken ?? ""
        print("FCM token: \(token)")
        // [END log_fcm_reg_token]
        
        if tokenId != nil {
            token = tokenId!
        }
        
        JsonUpdateCustomer.Todo.token(tokenid: token) { (error) in
            // TokenId is Posted
        }
    }
    
    let locationManager = CLLocationManager()
    
    func getLocation(_ errorStatus: Bool = true) {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                
                if errorStatus {                    
                    AlertFunctions.messageType.showYesNoAlert("Konum Kapalı", bodyMessage: "Sana yakın mağazaları görebilmen için konumuna ihtiyacımız var.\nAyarlar>Gizlilik>Konum Servisleri'nden konumunu açmak ister misin?", {
                        if let url = URL(string:"App-prefs:root=Privacy") {
                            if UIApplication.shared.canOpenURL(url) {
                                if #available(iOS 10.0, *) {
                                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                } else {
                                    UIApplication.shared.openURL(url)
                                }
                            }
                        }
                    }) {
                        
                    }
                    
                    
                }
                
            case .authorizedAlways, .authorizedWhenInUse:
                break
//                print("Access")
            }
        } else {
            print("Location services are not enabled")
        }
        
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest // Keskinlik
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let l = CLLocationCoordinate2D(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude)
        
        CLGeocoder().reverseGeocodeLocation(manager.location!) { (placemarks, error) in
            if error == nil && placemarks!.count != 0 {
                self.locationManager.stopUpdatingLocation()
                let placemark = placemarks![0] as CLPlacemark
                let locality = placemark.locality ?? placemark.administrativeArea
                let city = placemark.administrativeArea

                let location = GlobalVariables.Location(latitude: l.latitude, longitude: l.longitude, city: city, locality: locality)
                
                // Save
                if let encoded = try? JSONEncoder().encode(location) {
                    UserDefaults.standard.set(encoded, forKey: "location")
                    UserDefaults.standard.synchronize()
                    print(location)
                }
                
//                print(placemark.locality!)  // Edirne Merkez
//                print(placemark.postalCode!) // 22100
//                print(placemark.administrativeArea!) // Edirne
//                print(placemark.country!) // Turkiye
                
            }
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error Location", error.localizedDescription)
        
        UserDefaults.standard.removeObject(forKey: "location")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentLocation() -> GlobalVariables.Location? {
        // Get Location
        if let locData = UserDefaults.standard.object(forKey: "location") as? Data {
            if let location = try? JSONDecoder().decode(GlobalVariables.Location.self, from: locData) {
                getLocation()
                return location
            }
        }
        return nil
    }
    
    // MASK: Animations
    let duration: TimeInterval = 0.5
    let delay: TimeInterval = 0.0
    let usingSpringWithDamping: CGFloat = 0.3
    let initialSpringVelocity: CGFloat = 8
    
    func animationShake<T: UIView>(_ obj: T, _ times: CGFloat = 12) {
        // sgm = 12
        // button = 12
        // image = 12
        // label = 6
        // view = 10
        DispatchQueue.main.async() {
            let bounds = obj.bounds
            UIView.animate(withDuration: self.duration, delay: self.delay, usingSpringWithDamping: self.usingSpringWithDamping, initialSpringVelocity: self.initialSpringVelocity, options: UIViewAnimationOptions(), animations: {
                
                obj.bounds = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width+(bounds.size.width/times), height: bounds.size.height+(bounds.size.width/times))
                
            }, completion: nil)
            
            obj.bounds = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width, height: bounds.size.height)
        }
    }
    
    class func isValidEmail(_ email:String) -> Bool {
        let emailRegEx =
            "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
                + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
                + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
                + "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
                + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
                + "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
                + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
//        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)

        let result = emailTest.evaluate(with: email)
        
        if result == false {
            AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Geçerli bir E-Posta adresi girmelisin")
        }
        
        return result
    }
    
    class func isValidPhone(_ value: String, _ error: Bool = true) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        var result =  phoneTest.evaluate(with: value)
        
//        if value.count != 11 {
//            result = false
//        }
        
        if Int(value) == 0 {
            result = false
        }
        
        if result == false && error {
            AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Geçerli bir cep telefonu numarası girmelisin")
        }        
        
        return result
    }
    
    // MASK: Navigation Controller New Bar Button
    func newBarButton(_ imgName: String, action: Selector, view: UIViewController) -> UIBarButtonItem{
        let button: UIButton = UIButton(type: .custom)
        button.setImage(UIImage(named: imgName), for: UIControlState())
//        button.frame = CGRect(x: 20,y: 20,width: 23,height: 32)
        //        button.bounds = CGRect(x: 0,y: 70,width: 30,height: 80)
        button.bounds = CGRect(x: 20,y: 20,width: 23,height: 32)
        
        button.addTarget(view, action: action, for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: button)
        
        return barButton
    }
    
    class func getTopController() -> UIViewController {
        var topController = UIApplication.shared.keyWindow!.rootViewController! as UIViewController
        while ((topController.presentedViewController) != nil) {
            topController = topController.presentedViewController!;
        }
        return topController
    }
    
}



