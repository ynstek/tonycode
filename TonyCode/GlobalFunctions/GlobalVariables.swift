//
//  GlobalVariables.swift
//  Orca POS Mobile
//
//  Created by Yunus TEK on 21.03.2017.
//  Copyright © 2017 Orca Businesss Solutions. All rights reserved.
//

import Foundation
import UIKit

private let _sharedGlobalVariables = GlobalVariables()
class GlobalVariables : NSObject  {
    
    // MARK: - SHARED INSTANCE
    class var shared : GlobalVariables {
        return _sharedGlobalVariables
    }
    
    lazy var cardInfo : JsonPay.CardInfo? = nil
    var distance: Int = -1
    
    var searchSeller = ""
    
    struct Location : Codable {
        var latitude : Double?
        var longitude : Double?
        var city : String?
        var locality : String?
    }
    
    lazy var selectedBildirim : JsonBildirim.Response? = nil
    lazy var jsonBildirim : [JsonBildirim.Response] = {
        return [JsonBildirim.Response]()
    }()
    
    lazy var jsonPayment : [JsonPayment.Response] = {
        return [JsonPayment.Response]()
    }()
    
    lazy var jsonHistory : [JsonHistory.Response] = {
        return [JsonHistory.Response]()
    }()
    
    lazy var currentCustomer : JsonGetCustomer.Response? = nil
    
    lazy var jsonCategory : [JsonCategory.Response] = {
        return [JsonCategory.Response]()
    }()
    
    lazy var jsonSellerList : [JsonSeller.Response] = {
        return [JsonSeller.Response]()
    }()
    
    lazy var selectedSeller : JsonSeller.Response? = nil
    lazy var selectedSellerId : Int = -1
    lazy var listLimit : Int = 5
    lazy var selectedSellerDetail : JsonSellerDetail.Response? = nil
    
    lazy var jsonFavoriteList : [JsonSeller.Response] = {
        return [JsonSeller.Response]()
    }()
    
    
    lazy var jsonCity : [JsonCity.Response] = {
        return [JsonCity.Response]()
    }()
    
    lazy var jsonCountryCode : [JsonCountryCode.Response] = {
        return [JsonCountryCode.Response]()
    }()
    
    let url = "https://tonycode.net"
}


