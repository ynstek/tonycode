//
//  JsonBarcode.swift
//  TonyCode
//
//  Created by Yunus Tek on 24.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonBarcode : NSObject  {
    struct Value : Codable {
        let result : String?
        let error_message : String?
        let qrcode : String?
        let qrnumber : String?
    }
    
    static func endpointForTodos() -> String {
        return GlobalVariables.shared.url + "/api/barcode"
    }
    
    struct Todo: Codable {
        // POST - SAVE
        static func connect(completionHandler: @escaping (Value?, Error?) -> Void) {
            let endpoint = endpointForTodos()
            
            var oldcode = ""
            if let oc = UserDefaults.standard.value(forKey: "oldcode") as? String {
                oldcode = oc
            }
            
            let params = [ "customerid" : GlobalVariables.shared.currentCustomer!.id!
                ,"oldcode" : oldcode
                ] as [String : Any]
            
            guard let todosURL = URL(string: endpoint) else {
                let error = Json.BackendError.urlError(reason: "Could not create URL")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = Json.BackendError.urlError(reason: "Error httpBody")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let data = data {
                    do {
                        guard (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any]) != nil else { return }
                        
                        let todos = try JSONDecoder().decode(Value.self, from: data)
                        UserDefaults.standard.set(todos.qrnumber, forKey: "oldcode")
                        UserDefaults.standard.synchronize()
                        
                        completionHandler(todos, nil)
                        
                        return
                    } catch let error {
                        AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                        completionHandler(nil, error)
                        return
                        
                    }
                }
                }.resume()
        }
    }
}
