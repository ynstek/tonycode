
//
//  JsonCity.swift
//  TonyCode
//
//  Created by Yunus Tek on 17.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonCity : NSObject  {
    
    struct Value : Codable {
        let result : String?
        let response : [Response]?
    }
    
    struct Response : Codable {
        let id : Int?
        let baslik : String?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id) ?? -1
            baslik = try values.decodeIfPresent(String.self, forKey: .baslik) ?? ""
        }
    }
    
    static func endpointForTodos() -> String {
        return GlobalVariables.shared.url + "/api/citylist"
    }
    
    struct Todo: Codable {
        static func connect(completionHandler: @escaping ([Response]?, Error?) -> Void) {
            let endpoint = endpointForTodos()
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Could not construct URL")
                let error = Json.BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {(data, response, error) in
                guard data != nil else {
                    print("Error: did not receive data", error!)
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Did not receive data\n" + error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                
                guard error == nil else {
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                
                do {
                    guard let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String : Any] else { return }

                    if (json["result"] as? String)?.lowercased() == "success" {
                        let todos = try JSONDecoder().decode(Value.self, from: data!)
                        
                        UserDefaults.standard.set(try? PropertyListEncoder().encode(todos.response), forKey: "jsonCity")
                        UserDefaults.standard.synchronize()
                        GlobalVariables.shared.jsonCity = todos.response!
                        
                        completionHandler(todos.response , nil)
                        return
                    } else {
                        completionHandler(nil , nil)
                        return
                    }
                    
                } catch {
                    print("Error trying to convert data to JSON")
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error.localizedDescription)
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
        
    }
}

