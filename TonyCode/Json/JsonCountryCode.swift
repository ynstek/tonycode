//
//  JsonCountryCode.swift
//  TonyCode
//
//  Created by Yunus Tek on 22.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonCountryCode : NSObject  {
    
    struct Value : Codable {
        let response : [Response]?
    }
    
    struct Response : Codable {
        let tel : String?
        let code : String?
        let format : String?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            tel = try values.decodeIfPresent(String.self, forKey: .tel) ?? ""
            code = try values.decodeIfPresent(String.self, forKey: .code) ?? ""
            format = try values.decodeIfPresent(String.self, forKey: .format)
        }
    }
    
    static func endpointForTodos() -> String {
        return GlobalVariables.shared.url + "/api/list/ulke"
    }
    
    struct Todo: Codable {
        static func connect(completionHandler: @escaping ([Response]?, Error?) -> Void) {
            let endpoint = endpointForTodos()
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Could not construct URL")
                let error = Json.BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {(data, response, error) in
                guard data != nil else {
                    print("Error: did not receive data", error!)
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Did not receive data\n" + error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                
                guard error == nil else {
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                
                do {
                    guard (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String : Any]) != nil else { return }
                    
                    let todos = try JSONDecoder().decode(Value.self, from: data!)
                    
                    GlobalVariables.shared.jsonCountryCode = todos.response!
                    
                    completionHandler(todos.response , nil)
                    return
                    
                } catch {
                    print("Error trying to convert data to JSON")
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error.localizedDescription)
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
        
    }
}

