//
//  JsonFeedback.swift
//  SakaryaKampanyalar
//
//  Created by Yunus Tek on 16.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonGetCustomer : NSObject  {
    
    struct Response : Codable {
        let id : Int?
        let phone : String?
        let name : String?
        let lastname : String?
        let city : String?
        let gender : String?
        let email : String?
        let profil_foto : String?
        let birthdate : String?
        let sure: String?
        let tc: String?
        
        init(json: [String : Any]) {
            id = json["id"] as? Int ?? -1
            phone = json["phone"] as? String ?? ""
            name = json["name"] as? String ?? ""
            lastname = json["lastname"] as? String ?? ""
            city = json["city"] as? String ?? ""
            gender = json["gender"] as? String ?? ""
            email = json["email"] as? String ?? ""
            profil_foto = json["profil_foto"] as? String ?? ""
            birthdate = json["birthdate"] as? String ?? ""
            sure = json["sure"] as? String ?? ""
            tc = json["tc"] as? String ?? ""
        }
    }

    static func controlUrl() -> String {
        return GlobalVariables.shared.url + "/api/controlcustomer"
    }
    
    static func detailUrl() -> String {
        return GlobalVariables.shared.url + "/api/detailcustomer"
    }
    
    
    struct Todo: Codable {
        // POST - SAVE
        static func connect(phone: String? = nil, id: Int? = nil, completionHandler: @escaping (Response?, Error?) -> Void) {
            var endpoint = detailUrl()
            
            var params = [ : ] as [String : Any]
            
            if phone != nil {
                endpoint = controlUrl()
                params["phone"] = phone!
            } else {
                params["id"] = id!
            }
            
            guard let todosURL = URL(string: endpoint) else {
                let error = Json.BackendError.urlError(reason: "Could not create URL")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
           
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = Json.BackendError.urlError(reason: "Error httpBody")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let data = data {
                    do {
                        guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] else { return }
                        
                        if (json["result"] as? String)?.lowercased() == "success" {
                            let todos = Response(json: json["response"] as! [String : Any])
                            GlobalVariables.shared.currentCustomer = todos
                            
                            completionHandler(todos, nil)
                            return
                        } else { // Error
                            completionHandler(nil, nil)
                            return
                        }
                    } catch let error {
                        AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                        completionHandler(nil, error)
                        return
                    }
                }
                }.resume()
        }
    }
}
