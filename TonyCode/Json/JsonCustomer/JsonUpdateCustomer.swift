//
//  JsonUpdateCustomer.swift
//  TonyCode
//
//  Created by Yunus Tek on 17.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonUpdateCustomer : NSObject  {
    
    struct Value: Codable {
        let result : String?
        let error_message : String?
        
        init(json: [String : Any]) {
            result = json["result"] as? String ?? ""
            error_message = json["error_message"] as? String ?? ""
        }
    }
    
    static func setTokenUrl() -> String {
        return GlobalVariables.shared.url + "/api/customer/token"
    }
    
    static func updateUrl() -> String {
        return GlobalVariables.shared.url + "/api/editcustomer"
    }
    
    static func createUrl() -> String {
        return GlobalVariables.shared.url + "/api/newcustomer"
    }
    
    struct Todo: Codable {
        // POST - SAVE
        static func connect(_ phone: String? = nil, name: String, lastname: String, email: String, city: String, gender: String, birthdate: String, profil_photo: String?, tc: String, completionHandler: @escaping (Bool?, Error?, String?) -> Void) {
            
            var params =
                [
                    "name" : name
                    ,"lastname" : lastname
                    ,"email" : email
                    ,"city" : city
                    ,"gender" : gender
                    ,"birthdate" : birthdate
                    ,"tc" : tc
                    ] as [String : Any]
            if profil_photo != nil {
                 params["profil_foto"] = profil_photo
            }
            
            var endpoint = updateUrl()
            
            if GlobalVariables.shared.currentCustomer != nil { // Update
                params["id"] = GlobalVariables.shared.currentCustomer!.id!
            } else { // New
                params["phone"] = phone
                endpoint = createUrl()
            }
            
            guard let todosURL = URL(string: endpoint) else {
                let error = Json.BackendError.urlError(reason: "Could not create URL")
                completionHandler(nil, error, nil)
                return
            }
        
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: .init(rawValue: 0)) else {
                let error = Json.BackendError.urlError(reason: "Error httpBody")
                completionHandler(nil, error, nil)
                return
            }
        
            todosUrlRequest.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let data = data {
                    do {
                        guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] else { return }
                        
                        if (json["result"] as? String)?.lowercased() == "success" {
                            let todos = JsonGetCustomer.Response(json: json["response"] as! [String : Any])
                            GlobalVariables.shared.currentCustomer = todos
                            completionHandler(true, nil, nil)
                            return
                            
                        } else if json["result"] as? String == "Error" { // Error
                            completionHandler(false, nil, json["error_message"] as? String)
                            return
                        } else {
                            let error = Json.BackendError.urlError(reason: "Error")
                            completionHandler(nil, error, nil)
                            return
                        }
                        
                    } catch let error {
                        print(error.localizedDescription)
                        completionHandler(nil, error, nil)
                        return
                    }
                }
                }.resume()
        }
        
        static func token(tokenid: String, completionHandler: @escaping (Error?) -> Void) {
            let endpoint = setTokenUrl()
            
            let customerid = GlobalVariables.shared.currentCustomer?.id! ?? 0
            let params = [
                "customerid" : customerid
                ,"tokenid" : tokenid
                ] as [String : Any]
            
            guard let todosURL = URL(string: endpoint) else {
                let error = Json.BackendError.urlError(reason: "Could not create URL")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = Json.BackendError.urlError(reason: "Error httpBody")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
//                if let data = data {
//                    do {
//                        guard (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any]) != nil else { return }

                        completionHandler(nil)

//                        if (json["result"] as? String)?.lowercased() == "success" {
//                            let todos = try JSONDecoder().decode(Value.self, from: data)
//                            completionHandler(nil)
//                            return
//                        } else { // Error
//                            completionHandler(nil)
//                            return
//                        }
                        
//                    } catch let error {
//                        // Calisiyor ama format farkli oldugu icin hata veriyor onemli degil.
////                        AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
//                        completionHandler(error)
//                        return
//                    }
//                }
                }.resume()
        }
        
    }
}
