//
//  JsonFavorite.swift
//  TonyCode
//
//  Created by Yunus Tek on 25.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonFavorite : NSObject  {
    
    static func endpointForTodos() -> String {
        return GlobalVariables.shared.url + "/api/favorite"
    }
    
    struct Todo: Codable {
        // POST - SAVE
        static func connect(seller_id: Int, completionHandler: @escaping (Bool?, Error?) -> Void) {
            let endpoint = endpointForTodos()
            
            let params = [
                "customerid" : GlobalVariables.shared.currentCustomer!.id!
                , "seller_id" : seller_id
                ] as [String : Any]
            
            guard let todosURL = URL(string: endpoint) else {
                let error = Json.BackendError.urlError(reason: "Could not create URL")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = Json.BackendError.urlError(reason: "Error httpBody")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let data = data {
                    do {
                        guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] else { return }
                        
                        if json["response"] as? Bool == true {
                            completionHandler(true, nil)
                            return
                        } else {
                            completionHandler(false, nil)
                            return
                        }
                        
                    } catch let error {
                        AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                        completionHandler(nil, error)
                        return
                    }
                }
                }.resume()
        }
    }
}
