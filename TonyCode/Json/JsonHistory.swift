//
//  JsonHistory.swift
//  TonyCode
//
//  Created by Yunus Tek on 4.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonHistory : NSObject  {
    
    struct Value : Codable {
        let result : String?
        let response : [Response]?
        let sum : Float?
    }
    
    struct Response : Codable {
        let priceTotal : Float?
        let discountPrice : Float?
        let sellerName : String?
        let categoryName : String?
        let tonyDiscount : Float?
        let usePaymentDate : String?

        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            priceTotal = try values.decodeIfPresent(Float.self, forKey: .priceTotal) ?? -1
            discountPrice = try values.decodeIfPresent(Float.self, forKey: .discountPrice) ?? -1
            sellerName = try values.decodeIfPresent(String.self, forKey: .sellerName) ?? ""
            categoryName = try values.decodeIfPresent(String.self, forKey: .categoryName) ?? ""
            tonyDiscount = try values.decodeIfPresent(Float.self, forKey: .tonyDiscount) ?? -1
            usePaymentDate = try values.decodeIfPresent(String.self, forKey: .usePaymentDate) ?? ""
        }
    }
    
    static func endpointForTodos() -> String {
        return GlobalVariables.shared.url + "/api/history/list"
    }
    
    struct Todo: Codable {
        static func connect(time: Int, completionHandler: @escaping (Value?, Error?) -> Void) {
            let endpoint = endpointForTodos()
            
            let customerid = GlobalVariables.shared.currentCustomer!.id!
            let params = [
                "time" : time,
                "customerid" : customerid
                ] as [String : Any]
            
            guard let todosURL = URL(string: endpoint) else {
                let error = Json.BackendError.urlError(reason: "Could not create URL")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = Json.BackendError.urlError(reason: "Error httpBody")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let data = data {
                    do {
                        guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] else { return }
                        
                        if (json["result"] as? String)?.lowercased() == "success" {
                            let todos = try JSONDecoder().decode(Value.self, from: data)
                            completionHandler(todos, nil)
                            return
                        } else { // Error
                            completionHandler(nil, nil)
                            return
                        }
                        
                    } catch let error {
                        AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                        completionHandler(nil, error)
                        return
                        
                    }
                }
                }.resume()
        }
    }
}
