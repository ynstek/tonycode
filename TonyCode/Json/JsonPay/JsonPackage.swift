//
//  JsonPackage.swift
//  TonyCode
//
//  Created by Yunus Tek on 31.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonPackage : NSObject  {
    
    struct Value : Codable {
        let result : String?
        let response : [Response]?
    }
    
    struct Response : Codable {
        let id : Int?
        let time : String?
        let price : String?

        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id) ?? -1
            time = try values.decodeIfPresent(String.self, forKey: .time) ?? ""
            price = try values.decodeIfPresent(String.self, forKey: .price) ?? ""
        }
    }
    
    static func endpointForTodos() -> String {
        return GlobalVariables.shared.url + "/api/packages"
    }
    
    // GET
    struct Todo: Codable {
        static func connect(completionHandler: @escaping ([Response]?, Error?) -> Void) {
            let endpoint = endpointForTodos()
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Could not construct URL")
                let error = Json.BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {(data, response, error) in
                guard data != nil else {
                    print("Error: did not receive data", error!)
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Did not receive data\n" + error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                
                guard error == nil else {
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                
                do {
                    guard let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String : Any] else { return }
                    
                    if (json["result"] as? String)?.lowercased() == "success" {
                        let todos = try JSONDecoder().decode(Value.self, from: data!)
                        
                        completionHandler(todos.response , nil)
                        return
                    } else {
                        completionHandler(nil , nil)
                        return
                    }
                    
                } catch {
                    print("Error trying to convert data to JSON")
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error.localizedDescription)
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
        
    }
}

