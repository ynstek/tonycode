//
//  JsonPay.swift
//  TonyCode
//
//  Created by Yunus Tek on 29.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonPay : NSObject  {
    
    struct Value : Codable {
        let result : String?
        let response : String?
    }
    
    struct CardInfo : Codable {
        var packageid : Int?
        var holdername : String?
        var cardnumber : String?
        var month : String?
        var year : String?
        var cvc : String?
        
        var htmlCode : String? = ""
        
        var errorMessage: String? = ""
        var success : Bool? = nil
    }
    
    static func odemeUrl() -> String {
        return GlobalVariables.shared.url + "/api/odeme"
    }
    
    static func odemeControlUrl() -> String {
        return GlobalVariables.shared.url + "/api/odeme/control"
    }
    
    struct Todo: Codable {
        static func odeme(completionHandler: @escaping (Value?, Error?) -> Void) {
            let endpoint = odemeUrl()
            
            let cardInfo = GlobalVariables.shared.cardInfo!
            let params = [
            "customerid" : GlobalVariables.shared.currentCustomer!.id!
                ,"packageid" : cardInfo.packageid!
                ,"holdername" : cardInfo.holdername!
                ,"cardnumber" : cardInfo.cardnumber!
                ,"month" : cardInfo.month!
                ,"year" : cardInfo.year!
                ,"cvc" : cardInfo.cvc!
                ] as [String : Any]
            
            guard let todosURL = URL(string: endpoint) else {
                let error = Json.BackendError.urlError(reason: "Could not create URL")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = Json.BackendError.urlError(reason: "Error httpBody")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let data = data {
                    do {
                        guard (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any]) != nil else { return }
                        
                        let todos = try JSONDecoder().decode(Value.self, from: data)
                        completionHandler(todos, nil)
                        return
                        
                    } catch let error {
                        AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                        completionHandler(nil, error)
                        return
                    }
                }
                }.resume()
        }

        
        static func control(controlHtml: String, completionHandler: @escaping (Value?, Error?) -> Void) {
            
            let data: Data = controlHtml.data(using: .utf8)!
            
            do {
                guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] else { return }
                
                print(json)
                
                let todos = try JSONDecoder().decode(Value.self, from: data)
                
                completionHandler(todos, nil)
                
                return
                
            } catch let error {
                completionHandler(nil, error)
                return
            }
        }
        
        static func odemeControl(threeparseData: String, completionHandler: @escaping (Value?, Error?) -> Void) {
            let endpoint = odemeControlUrl()

            let params = [ "threeparseData" : threeparseData ] as [String : Any]
            
            guard let todosURL = URL(string: endpoint) else {
                let error = Json.BackendError.urlError(reason: "Could not create URL")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = Json.BackendError.urlError(reason: "Error httpBody")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody

            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let data = data {
                    do {
                        guard (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any]) != nil else { return }
                        
                        let todos = try JSONDecoder().decode(Value.self, from: data)
                        
                        completionHandler(todos, nil)
                        
                        return
                        
                    } catch let error {
                        AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                        completionHandler(nil, error)
                        return
                    }
                }
                }.resume()
        }
        
    }
}
