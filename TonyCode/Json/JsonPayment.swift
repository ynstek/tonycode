//
//  JsonPayment.swift
//  TonyCode
//
//  Created by Yunus Tek on 5.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation


class JsonPayment : NSObject  {
    
    struct Value : Codable {
        let result : String?
        let response : [Response]?
    }
    
    struct Response : Codable {
        let time : String?
        let paymentDate : String?
        let price : String?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            time = try values.decodeIfPresent(String.self, forKey: .time) ?? ""
            paymentDate = try values.decodeIfPresent(String.self, forKey: .paymentDate) ?? ""
            price = try values.decodeIfPresent(String.self, forKey: .price) ?? ""
        }
    }
    
    static func endpointForTodos() -> String {
        return GlobalVariables.shared.url + "/api/payments"
    }
    
    struct Todo: Codable {
        // POST - SAVE
        static func connect(completionHandler: @escaping ([Response]?, Error?) -> Void) {
            let endpoint = endpointForTodos()
            
            let customerid = GlobalVariables.shared.currentCustomer!.id!
            let params = [
                "customerid" : customerid
                ] as [String : Any]
            
            guard let todosURL = URL(string: endpoint) else {
                let error = Json.BackendError.urlError(reason: "Could not create URL")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = Json.BackendError.urlError(reason: "Error httpBody")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let data = data {
                    do {
                        guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] else { return }
                        
                        if (json["result"] as? String)?.lowercased() == "success" {
                            let todos = try JSONDecoder().decode(Value.self, from: data)
                            completionHandler(todos.response, nil)
                            return
                        } else { // Error
                            completionHandler(nil, nil)
                            return
                        }
                        
                    } catch let error {
                        AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                        completionHandler(nil, error)
                        return
                        
                    }
                }
                }.resume()
        }
    }
}
