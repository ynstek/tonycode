//
//  JsonSeller.swift
//  TonyCode
//
//  Created by Yunus Tek on 18.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonSeller : NSObject  {
    
    struct Value : Codable {
        let result : String?
        var response : [Response]?
    }
    
    struct Response : Codable {
        var id : Int?
        let name : String?
        let photo : String?
        let cat_icon : String?
        let discount : Int?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id) ?? -1
            name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
            photo = try values.decodeIfPresent(String.self, forKey: .photo) ?? ""
            cat_icon = try values.decodeIfPresent(String.self, forKey: .cat_icon) ?? ""
            discount = try values.decodeIfPresent(Int.self, forKey: .discount) ?? 0
        }
    }
    
    static func sellerUrl() -> String {
        return GlobalVariables.shared.url + "/api/sellerlist"
    }
    
    static func favoriteUrl() -> String {
        return GlobalVariables.shared.url + "/api/favorite/list"
    }
    
    struct Todo: Codable {
        // POST - SAVE
        static func seller(cat: Int = -1, pageno: Int = 0, completionHandler: @escaping ([Response]?, Error?) -> Void) {
            
            let endpoint = sellerUrl()
            
            var province = GlobalVariables.shared.currentCustomer!.city!
            var lat: Double = -1
            var long: Double = -1
            
            if let location = GlobalFunctions.shared.getCurrentLocation() {
                province = location.city!
                lat = location.latitude!
                long = location.longitude!
            }
                        
            let params = [
                  "province" : province
                   ,"pageno" : pageno
                     , "cat" : cat
                , "distance" : GlobalVariables.shared.distance
                , "lat" : lat
                , "long" : long
                , "search" : GlobalVariables.shared.searchSeller
                ] as [String : Any]
            
            guard let todosURL = URL(string: endpoint) else {
                let error = Json.BackendError.urlError(reason: "Could not create URL")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = Json.BackendError.urlError(reason: "Error httpBody")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let data = data {
                    do {
                        guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] else { return }
                        
                     
                        if (json["result"] as? String)?.lowercased() == "success" {
                            let todos = try JSONDecoder().decode(Value.self, from: data)
                            
                            if let limit = json["limit"] as? Int {
                                GlobalVariables.shared.listLimit = limit
                            }
                            
                            completionHandler(todos.response, nil)
                            return
                        } else { // Error
                            completionHandler(nil, nil)
                            return
                        }
                        
                    } catch let error {
                        AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                        completionHandler(nil, error)
                        return
                    }
                }
                }.resume()
        }
        
        static func favorite(pageno: Int = 0, completionHandler: @escaping ([Response]?, Error?) -> Void) {
            
            let endpoint = favoriteUrl()
            
            let params = [
                "customerid" : GlobalVariables.shared.currentCustomer!.id!
                ,"pageno" : pageno
                ] as [String : Any]
            
            guard let todosURL = URL(string: endpoint) else {
                let error = Json.BackendError.urlError(reason: "Could not create URL")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = Json.BackendError.urlError(reason: "Error httpBody")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let data = data {
                    do {
                        guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] else { return }
                        
                        
                        if (json["result"] as? String)?.lowercased() == "success" {
                            let todos = try JSONDecoder().decode(Value.self, from: data)
                            
                            if let limit = json["limit"] as? Int {
                                GlobalVariables.shared.listLimit = limit
                            }
                            
                            completionHandler(todos.response, nil)
                            return
                        } else { // Error
                            completionHandler(nil, nil)
                            return
                        }
                        
                    } catch let error {
                        AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                        completionHandler(nil, error)
                        return
                    }
                }
                }.resume()
        }
        
    }
    
    
}
