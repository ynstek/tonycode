//
//  JsonSellerDetail.swift
//  TonyCode
//
//  Created by Yunus Tek on 21.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonSellerDetail : NSObject  {
    struct Value : Codable {
        let result : String?
        var response : Response?
    }
    
    struct Response : Codable {
        var id : Int?
        let name : String?
        let desc : String?
        let latitude : String?
        let longitude : String?
        let phone : String?
        let adres : String?
        
        let opening_date : String?
        let closing_date : String?
        let photos : [Photos]?
        let comments : [Comments]?
        let favorite : Int?
        let discount : Int?
        let sartlar : String?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id) ?? -1
            name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
            desc = try values.decodeIfPresent(String.self, forKey: .desc) ?? ""
            latitude = try values.decodeIfPresent(String.self, forKey: .latitude) ?? "0"
            longitude = try values.decodeIfPresent(String.self, forKey: .longitude) ?? "0"
            phone = try values.decodeIfPresent(String.self, forKey: .phone) ?? ""
            adres = try values.decodeIfPresent(String.self, forKey: .adres) ?? ""
            
            opening_date = try values.decodeIfPresent(String.self, forKey: .opening_date) ?? ""
            closing_date = try values.decodeIfPresent(String.self, forKey: .closing_date) ?? ""
            photos = try values.decodeIfPresent([Photos].self, forKey: .photos)
            comments = try values.decodeIfPresent([Comments].self, forKey: .comments)
            favorite = try values.decodeIfPresent(Int.self, forKey: .favorite) ?? -1
            discount = try values.decodeIfPresent(Int.self, forKey: .discount) ?? 0
            sartlar = try values.decodeIfPresent(String.self, forKey: .sartlar) ?? ""
        }
    }
    
    struct Photos : Codable {
        let photo : String?
    }
    
    struct Comments : Codable {
        let comment : String?
        let name : String?
        let lastname : String?
        let created_at : String?
    }
    
    static func endpointForTodos() -> String {
        return GlobalVariables.shared.url + "/api/sellerlist/detail"
    }
    
    struct Todo: Codable {
        // POST - SAVE
        static func connect(id: Int, completionHandler: @escaping (Response?, Error?) -> Void) {
            let endpoint = endpointForTodos()
            
            let customerid = GlobalVariables.shared.currentCustomer!.id!
            let params = [
                "id" : id,
                "customerid" : customerid
                ] as [String : Any]
            
            guard let todosURL = URL(string: endpoint) else {
                let error = Json.BackendError.urlError(reason: "Could not create URL")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = Json.BackendError.urlError(reason: "Error httpBody")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let data = data {
                    do {
                        guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] else { return }
                        
                        if (json["result"] as? String)?.lowercased() == "success" {
                            let todos = try JSONDecoder().decode(Value.self, from: data)
                            completionHandler(todos.response, nil)
                            return
                        } else { // Error
                            completionHandler(nil, nil)
                            return
                        }
                        
                    } catch let error {
                        AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                        completionHandler(nil, error)
                        return

                    }
                }
                }.resume()
        }
    }
}
