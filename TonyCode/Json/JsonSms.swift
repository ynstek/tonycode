//
//  JsonSms.swift
//  TonyCode
//
//  Created by Yunus Tek on 5.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation


class JsonSms : NSObject  {
    
    struct Value : Codable {
        let result : String?
        let response : JsonGetCustomer.Response?
        let error_message : String?
    }
    
    static func smsUrl() -> String {
        return GlobalVariables.shared.url + "/api/sms"
    }
    
    static func smsControlUrl() -> String {
        return GlobalVariables.shared.url + "/api/sms/control"
    }
    
    struct Todo: Codable {
        static func sms(phone: String, completionHandler: @escaping (Bool, Error?) -> Void) {
            let endpoint = smsUrl()
            
            let params = [
                "phone" : phone
                ] as [String : Any]
            
            guard let todosURL = URL(string: endpoint) else {
                let error = Json.BackendError.urlError(reason: "Could not create URL")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(false, error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = Json.BackendError.urlError(reason: "Error httpBody")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(false, error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let data = data {
                    do {
                        guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] else { return }
                        
                        if (json["result"] as? String)?.lowercased() == "success" {
//                            let todos = try JSONDecoder().decode(Value.self, from: data)
                            completionHandler(true, nil)
                            return
                        } else { // Error
                            completionHandler(false, nil)
                            AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: "Bir hata oluştu. Lütfen tekrar deneyiniz.")
                            return
                        }
                    } catch let error {
                        AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                        completionHandler(false, error)
                        return
                        
                    }
                }
                }.resume()
        }
        
        static func control(phone: String, code: String, completionHandler: @escaping (Value?, Error?) -> Void) {
            let endpoint = smsControlUrl()
            
            let params = [
                "phone" : phone
                ,"code" : code
                ] as [String : Any]
            
            guard let todosURL = URL(string: endpoint) else {
                let error = Json.BackendError.urlError(reason: "Could not create URL")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = Json.BackendError.urlError(reason: "Error httpBody")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let data = data {
                    do {
                        guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] else { return }
                        
                        print(json)
                        
                        let todos = try JSONDecoder().decode(Value.self, from: data)
                        completionHandler(todos, nil)
                        return
                        
                    } catch let error {
                        AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                        completionHandler(nil, error)
                        return
                        
                    }
                }
                }.resume()
        }
        
    }
}
