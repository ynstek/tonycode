//
//  BarcodeVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 24.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class BarcodeVC: UIViewController {

    @IBOutlet weak var allView: UIView!
    @IBOutlet weak var top: NSLayoutConstraint!
    @IBOutlet weak var bottom: NSLayoutConstraint!

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgBarcode: UIImageView!
    @IBOutlet weak var lblBarcode: UILabel!
    
    var seconds = 0
    var timer = Timer()
    let maxSecond = 180

    var isDisappear = false

    @IBOutlet weak var bosView: UIView!
    @IBOutlet weak var lblSure: UILabel!

    @IBOutlet weak var bosHeader: NSLayoutConstraint!
    @IBOutlet weak var HeaderTop: NSLayoutConstraint!
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11, *) {
            HeaderTop.constant = 60 + view.safeAreaInsets.top - self.navigationController!.navigationBar.frame.height - 20
            bosHeader.constant = HeaderTop.constant
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presentTransparentNavigationBar()

        setContants()
    }
    
    fileprivate func setContants() {
        let height = allView.frame.height / 20
        top.constant = height
        bottom.constant = height
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.bosView.isHidden = true
        if timer.isValid == false {
            getNewBarcode()
        }
        isDisappear = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        isDisappear = true
    }
    
    @IBAction func btnPaketler(_ sender: Any) {
        tabBarController?.selectedIndex = 3 // Paketler
    }
    
    func getNewBarcode() {
        lblTime.text = "0"
        imgBarcode.image = nil
        lblBarcode.text = "Oluşturuluyor.."
        
        if Reachability.isConnectedToNetwork() {
            self.activityOpen()
            JsonBarcode.Todo.connect(completionHandler: { (result, error) in
                self.activityClose()
                if result!.result!.lowercased() == "success" {
                    DispatchQueue.main.async() {
                        self.bosView.isHidden = true
                        self.imgBarcode.downloadedFrom(link: result!.qrcode!)
                        self.lblBarcode.text = result!.qrnumber!
                        
                        self.runProgressRing()
                    }
                } else {
                    DispatchQueue.main.async() {
                        self.bosView.isHidden = false
                        if let customer = GlobalVariables.shared.currentCustomer {
                            self.lblSure.text = String(customer.sure!.ToDate().ToString())
                        }
                        //                    AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: result!.error_message!)
                    }
                }
            })
        }
    }
    
    func runProgressRing() {
        // Start Timer
        seconds = maxSecond
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(PhoneActivationVC.runTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func runTimer() {
        seconds -= 1
        self.lblTime.text = String(seconds)
        
        if seconds <= 0 {
            if isDisappear == false {
                getNewBarcode()
            } else {
                imgBarcode.image = nil
                lblBarcode.text = "Oluşturuluyor.."
                timer.invalidate()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
