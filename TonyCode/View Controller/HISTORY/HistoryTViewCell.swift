//
//  HistoryTViewCell.swift
//  TonyCode
//
//  Created by Yunus Tek on 4.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class HistoryTViewCell: UITableViewCell {

    @IBOutlet weak var priceTotal: UILabel!
    @IBOutlet weak var discountPrice: UILabel!
    @IBOutlet weak var tonyDiscount: UILabel!
    
    @IBOutlet weak var sellerName: UILabel!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var usePaymentDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
