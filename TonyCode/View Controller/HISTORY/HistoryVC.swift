//
//  HistoryVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 2.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class HistoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sum: UILabel!
    
    @IBOutlet weak var sgmHistory: UISegmentedControl!
    @IBOutlet weak var HeaderTop: NSLayoutConstraint!
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11, *) {
            HeaderTop.constant = 60 + view.safeAreaInsets.top - self.navigationController!.navigationBar.frame.height - 20
        }
    }
    
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshControlData), for: .valueChanged)
        return refreshControl
    }()
    
    @objc func refreshControlData() {
        self.getHistory(false)
        refreshControl.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getHistory()
        self.presentTransparentNavigationBar()
        self.tableView.insertSubview(self.refreshControl, at: 0)
    }
    
    @IBAction func sgmHistoryChange(_ sender: UISegmentedControl) {
        getHistory()
    }
    
    func getHistory(_ activity: Bool = true) {
        var time = 7
        switch sgmHistory.selectedSegmentIndex {
        case 1:
            time = 30
        case 2:
            time = 180
        default:
            time = 7
        }
        
        if Reachability.isConnectedToNetwork() {
            if activity {
                self.activityOpen()
            }
            JsonHistory.Todo.connect(time: time, completionHandler: { (result, error) in
                self.activityClose()
                if result != nil {
                    GlobalVariables.shared.jsonHistory = result!.response!
                    DispatchQueue.main.async() {
                        self.sum.text = String(result!.sum!) + " TL"
                        self.tableView.reloadData()
                    }
                }
            })
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = GlobalVariables.shared.jsonHistory.count
        tableView.isHidden = count == 0 ? true : false // For Bosview
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HistoryTViewCell
        let data = GlobalVariables.shared.jsonHistory[indexPath.row]
        
        clearCell(cell)
        
        cell.priceTotal.attributedText = (String(data.priceTotal!) + " TL").strikethrough()
        cell.discountPrice.text = String(data.discountPrice!) + " TL"
        cell.sellerName.text = String(data.sellerName!)
        cell.categoryName.text = String(data.categoryName!)
        cell.tonyDiscount.text = String(data.tonyDiscount!) + " TL"
        cell.usePaymentDate.text = data.usePaymentDate!.ToDate().ToString()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func clearCell(_ cell: HistoryTViewCell) {
        cell.priceTotal.text = ""
        cell.discountPrice.text = ""
        cell.sellerName.text = ""
        cell.categoryName.text = ""
        cell.tonyDiscount.text = ""
        cell.usePaymentDate.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
