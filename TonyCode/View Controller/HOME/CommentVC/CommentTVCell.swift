//
//  CommentTVCell.swift
//  TonyCode
//
//  Created by Yunus Tek on 24.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class CommentTVCell: UITableViewCell {

    
    @IBOutlet weak var lblBaslik: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var createDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
