//
//  CommentVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 24.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class CommentVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var txtYorum: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var HeaderTop: NSLayoutConstraint!
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11, *) {
            HeaderTop.constant = 60 + view.safeAreaInsets.top - self.navigationController!.navigationBar.frame.height - 20
        }
    }
    
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshControlData), for: .valueChanged)
        return refreshControl
    }()
    
    @objc func refreshControlData() {
        if Reachability.isConnectedToNetwork() {
            JsonSellerDetail.Todo.connect(id: GlobalVariables.shared.selectedSellerId) { (result, error) in
                if result != nil {
                    GlobalVariables.shared.selectedSellerDetail = result!
                    DispatchQueue.main.async() {
                        self.tableView.reloadData()
                        self.refreshControl.endRefreshing()
                    }
                }
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        borderView!.layer.cornerRadius = 18
        borderView!.layer.borderWidth = 2
        borderView!.layer.borderColor = UIColor.red.cgColor
        
        self.tableView.insertSubview(self.refreshControl, at: 0)
    }
    
    fileprivate func scrollToBottom() {
        DispatchQueue.main.async {
            if GlobalVariables.shared.selectedSellerDetail!.comments!.count != 0 {
                let indexPath = IndexPath(row: GlobalVariables.shared.selectedSellerDetail!.comments!.count-1, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    
    @IBAction func didChangeTxtYorum(_ sender: UITextField) {
        print(txtYorum.text!)
        if sender.text!.count > 105 {
            txtYorum.text = String(sender.text!.dropLast())
        }
        
        scrollToBottom()
    }
    
    @IBAction func btnYorumYap(_ sender: Any) {
        if txtYorum.text!.count > 3 {
            self.sendComment()
        } else {
            AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Yorumun 3 karakterden uzun olmalı.")
        }
    }
    
    func sendComment() {
        AlertFunctions.messageType.showYesNoAlert("Onay", bodyMessage: "Yorumunuz gönderilsin mi?", {
            if Reachability.isConnectedToNetwork() {
                self.activityOpen()
                JsonCommentSend.Todo.connect(seller_id: GlobalVariables.shared.selectedSellerId, comment: self.txtYorum.text!) { (result, error) in
                    self.activityClose()
                    if result != nil {
                        DispatchQueue.main.async() {
                            self.txtYorum.text = ""
                            AlertFunctions.messageType.showOKAlert("Yorum Gönderildi", bodyMessage: "Yorumun için teşekkürler. Onaylandıktan sonra yayınlanacaktır.")
                        }
                    }
                }
            }
        }) { // No
        
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.scrollToBottom()
        
        let count = GlobalVariables.shared.selectedSellerDetail!.comments!.count
        tableView.isHidden = count == 0 ? true : false // For Bosview
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CommentTVCell
        let data = GlobalVariables.shared.selectedSellerDetail!.comments![indexPath.row]
        
        cell.lblBaslik.text = data.name! + " " + data.lastname!
        cell.lblComment.text = data.comment!
        cell.createDate.text = data.created_at!.ToDate().ToString()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
