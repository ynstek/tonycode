//
//  DistanceVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 24.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit
import MapKit

class DistanceVC: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var lblMesafe: UILabel!
    @IBOutlet weak var lblKonum: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    let tumuMesaj = "Tüm il Mesafesi"
    let fullMap = 0.2
    
    @IBOutlet weak var HeaderTop: NSLayoutConstraint!
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11, *) {
            HeaderTop.constant = 60 + view.safeAreaInsets.top - self.navigationController!.navigationBar.frame.height - 20
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        slider.maximumValue = 7500
        slider.minimumValue = 100
        slider.value = slider.minimumValue
        lblMesafe.text = String(Int(slider.value)) + " m Mesafe"
        lblKonum.text = GlobalVariables.shared.currentCustomer!.city
        
        if let location = GlobalFunctions.shared.getCurrentLocation() {
            lblKonum.text = location.locality
        }
        
        if GlobalVariables.shared.distance != -1 {
            lblMesafe.text = String(GlobalVariables.shared.distance) + " m Mesafe"
            slider.value = Float(GlobalVariables.shared.distance)
            setMapLocation(CLLocationDegrees(slider.value/100000))
        } else {
            setMapLocation(fullMap)
        }
    }
    
    func setMapLocation(_ distance: CLLocationDegrees) {
        if let loc = GlobalFunctions.shared.getCurrentLocation() {
            slider.isEnabled = true
            if distance != fullMap {
                lblKonum.text = loc.locality
            } else {
                lblMesafe.text = tumuMesaj
                slider.value = slider.maximumValue
                lblKonum.text = loc.city
            }

            let lat = loc.latitude!
            let long = loc.longitude!
            
            let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
            
            let span = MKCoordinateSpan(latitudeDelta: distance, longitudeDelta: distance)
            let region = MKCoordinateRegion(center: location, span: span)
            mapView.setRegion(region, animated: true)
            
        } else {
            GlobalVariables.shared.distance = -1
            lblMesafe.text = tumuMesaj
            slider.value = slider.maximumValue
            lblKonum.text = GlobalVariables.shared.currentCustomer!.city
            GlobalFunctions.shared.getLocation()
            
            slider.isEnabled = false
        }
    }
    
    @IBAction func sliderChange(_ sender: UISlider) {
        let fixed = roundf(sender.value / 50.0) * 50.0;
        sender.setValue(fixed, animated: true)
        
        if sender.value == sender.maximumValue {
            setMapLocation(fullMap)
        } else {
            lblMesafe.text = sender.value.converToString() + " m Mesafe"
            setMapLocation(CLLocationDegrees(sender.value/100000))
            if let location = GlobalFunctions.shared.getCurrentLocation() {
                lblKonum.text = location.locality
            }
        }
    }
    
    @IBAction func btnMekanBul(_ sender: Any) {
        
        var newDistance = Int(slider.value)
        if slider.value == slider.maximumValue {
            newDistance = -1
        }
        
        if GlobalVariables.shared.distance != newDistance {
            GlobalVariables.shared.distance = newDistance
            NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadSeller"), object: nil)
        }

        // goBack
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
