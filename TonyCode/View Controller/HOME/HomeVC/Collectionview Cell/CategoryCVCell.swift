//
//  CategoryCVCell.swift
//  TonyCode
//
//  Created by Yunus Tek on 18.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class CategoryCVCell: UICollectionViewCell {
    
    @IBOutlet weak var viewCircle: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
}
