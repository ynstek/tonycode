//
//  SellerCVCell.swift
//  TonyCode
//
//  Created by Yunus Tek on 18.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class SellerCVCell: UICollectionViewCell {
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var name: UITextView!
    @IBOutlet weak var cat_icon: UIImageView!
    @IBOutlet weak var discount: UILabel!
    
}
