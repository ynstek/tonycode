//
//  SellerVC+handlers.swift
//  TonyCode
//
//  Created by Yunus Tek on 18.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation
import UIKit

extension HomeVC {
    
    func getData() {
        self.getCategory()
        self.getSeller()
    }
    
    func getCategory() {
        if Reachability.isConnectedToNetwork() {
            self.activityOpen()
            JsonCategory.Todo.connect(completionHandler: { (result, error) in
                self.activityClose()
                if result != nil {
                    DispatchQueue.main.async() {
                        self.categoryCV.reloadData()
                    }
                }
            })
        }
    }
    
    func getSeller(_ activity: Bool = true) {
        selectedPageNo = 0
        isExists = true
        if Reachability.isConnectedToNetwork() {
            if activity {
                self.activityOpen()
            }
            JsonSeller.Todo.seller(cat: selectedCat, pageno: selectedPageNo,completionHandler: { (result, error) in
                self.activityClose()
                if result != nil {
                    DispatchQueue.main.async() {
                        GlobalVariables.shared.jsonSellerList = result!
                        if result!.count != GlobalVariables.shared.listLimit {
                            self.isExists = false
                        }
                        self.sellerCV.reloadData()
                    }
                }
                DispatchQueue.main.async() {
                    self.refreshControl.endRefreshing()
                }
            })
        }
    }
    
    func loadNewSeller () {
        if isExists {
            selectedPageNo += 1
            
            if Reachability.isConnectedToNetwork() {
//                self.activityOpen()
                JsonSeller.Todo.seller(cat: selectedCat, pageno: selectedPageNo,completionHandler: { (result, error) in
                    self.activityClose()
                    if result != nil {
                        DispatchQueue.main.async() {
                            GlobalVariables.shared.jsonSellerList += result!
                            if result!.count != GlobalVariables.shared.listLimit {
                                self.isExists = false
                            }
                            self.sellerCV.reloadData()
                        }
                    }
                })
                
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    // MARK: CollectionView
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == categoryCV {
            return CGSize(width: 82, height: 93)
        } else { // sellerCV
            // MARK: View Size
            let count = CGFloat(GlobalVariables.shared.jsonSellerList.count)
            
            self.viewHeight.constant = 171 + (count * 180) + 20
            return CGSize(width: (self.view.frame.width - 20), height: 170)
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        if collectionView == sellerCV {
            switch kind {
            case UICollectionElementKindSectionHeader:
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: sellerReusable, for: indexPath) as! SellerCVReusable
                
                if selectedCat != -1 {
                    let category = GlobalVariables.shared.jsonCategory.filter() {
                        cat in
                        return cat.id == selectedCat
                    }[0]
                    
                    headerView.baslik.text = category.cat_name! + " Fırsatları"
                } else {
                    headerView.baslik.text = "Popüler Fırsatlar"
                }
                
                return headerView
            default:
                //                        assert(false, "Unexpected element kind")
                fatalError("Unexpected element kind")
            }
        } else {
            switch kind {
            case UICollectionElementKindSectionHeader:
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: categoryReusable, for: indexPath) as! CategoryCVReusable
                
                if selectedCat == -1 {
                    headerView.hepsiCircle.backgroundColor = UIColor.black
                } else {
                    headerView.hepsiCircle.backgroundColor = UIColor.red
                }
                
                return headerView
            default:
                //                        assert(false, "Unexpected element kind")
                fatalError("Unexpected element kind")
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoryCV {
            return GlobalVariables.shared.jsonCategory.count
        } else { // sellerCV
            self.viewHeight.constant = 171 + 20
            let count = GlobalVariables.shared.jsonSellerList.count
            
            bosText.isHidden = count != 0 ? true : false
            bosIcon.isHidden = count != 0 ? true : false
            
            collectionView.isHidden = count == 0 ? true : false
            
            return count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == categoryCV {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: categoryCell, for: indexPath) as! CategoryCVCell
            let data = GlobalVariables.shared.jsonCategory[indexPath.row]
            
            cell.viewCircle.backgroundColor = selectedCat == data.id ? UIColor.black : UIColor.red
            cell.lblName.text = data.cat_name
            cell.imgIcon.image = nil
            if let icon = data.cat_icon {
                cell.imgIcon.downloadedFrom(link: icon)
            }
            return cell
        } else { // sellerCV

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: sellerCell, for: indexPath) as! SellerCVCell
            
            let data = GlobalVariables.shared.jsonSellerList[indexPath.row]
            cell.name.text = data.name
            cell.photo.image = nil
            if let photo = data.photo {
                cell.photo.downloadedFrom(link: photo)
            }
            cell.cat_icon.image = nil
            if let icon = data.cat_icon {
                cell.cat_icon.downloadedFrom(link: icon)
            }
            
            cell.discount.text = "%" + String(data.discount!)

            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == categoryCV {
            let data = GlobalVariables.shared.jsonCategory[indexPath.row]

            if selectedCat == data.id {
                selectedCat = -1
            } else {
                selectedCat = data.id!
            }
            
            self.getSeller()
            categoryCV.reloadData()
        } else { // sellerCV
            GlobalVariables.shared.selectedSeller = GlobalVariables.shared.jsonSellerList[indexPath.row]
            GlobalVariables.shared.selectedSellerId = GlobalVariables.shared.selectedSeller!.id!
            self.backButton()
        }
    }
    
    func clearCategoryFilter() {
        selectedCat = -1
        
        self.getSeller()
        categoryCV.reloadData()
    }
    
    // Son restoranda Yeni restoran ekle
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == categoryCV {
            
        } else { // sellerCV
            // bunun yerine scroll kullanildi
//            if indexPath.row == GlobalVariables.shared.jsonSellerList.count - 1 {
//                loadNewSeller()
//            }
        }
    }
    
    // Son restoranda Yeni restoran ekle
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x == 0 && scrollView.contentOffset.y != 0{ // yana kaydirma yoksa
            if (scrollView.contentOffset.y == (scrollView.contentSize.height - scrollView.frame.size.height)) {
                //reach bottom
                loadNewSeller()
            }
        }
    }
    
}
