//
//  HomeVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 15.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class HomeVC: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var categoryCV: UICollectionView!
    let categoryCell = "categoryCell"
    let categoryReusable = "categoryReusable"
    @IBOutlet weak var sellerCV: UICollectionView!
    let sellerCell = "sellerCell"
    let sellerReusable = "sellerReusable"

    var selectedCat = -1
    var selectedPageNo = 0
    var isExists = true
    let searchBar = UISearchBar()
    
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshControlData), for: .valueChanged)
        return refreshControl
    }()
    
    @objc func refreshControlData() {
        self.getSeller(false)
    }
    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // Bos iken gelen view
    @IBOutlet weak var bosIcon: UIImageView!
    @IBOutlet weak var bosText: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GlobalVariables.shared.jsonSellerList.removeAll()

        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadSeller(_:)), name: NSNotification.Name(rawValue: "reloadSeller"), object: nil)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadSeller"), object: nil)
        
        GlobalFunctions.shared.getLocation(false)
        self.presentTransparentNavigationBar()
        createSearchBar()
        
        scrollView.insertSubview(self.refreshControl, at: 0)
    }
    
    @objc func OnDiscountClicked() {
        performSegue(withIdentifier: "discount", sender: nil)
    }
    
    @objc func OnLogoClicked() {
    }
    
    fileprivate func createSearchBar() {
        searchBar.showsCancelButton = false
        searchBar.placeholder = "Mağaza veya Kampanya Ara"
        searchBar.searchBarStyle = .minimal
        searchBar.delegate = self
    }
    
    fileprivate func createNavigation() {
        //        self.presentTransparentNavigationBar()
        self.navigationItem.titleView = searchBar
        
        self.navigationItem.rightBarButtonItem =
            GlobalFunctions.shared.newBarButton("pin", action: #selector(HomeVC.OnDiscountClicked), view: self)
        self.navigationItem.leftBarButtonItem =
            GlobalFunctions.shared.newBarButton("iconBarButton", action: #selector(HomeVC.OnLogoClicked), view: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        createNavigation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @objc func reloadSeller(_ notification: NSNotification) {
        if AppDelegate.isNofiticationSeller {
            AppDelegate.isNofiticationSeller = false
            performSegue(withIdentifier: "sellerDetail", sender: nil)
        } else if AppDelegate.isNofiticationBildirim {
            tabBarController?.selectedIndex = 4 // Profile
        }
        
        self.getData()
    }
    
    @IBAction func btnCategoryReusable(_ sender: Any) {
        clearCategoryFilter()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.backButton()
    }
    
    // MARK: SearchBar
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar.text!.count == 0 {
            GlobalVariables.shared.searchSeller = ""
            self.getSeller()
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text!.count >= 3 || searchBar.text!.count == 0 {
            self.dismissKeyBoard()
            self.getSeller()
        } else {
            AlertFunctions.messageType.showInfoAlert("", bodyMessage: "En az 3 karakter girmelisin")
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        GlobalVariables.shared.searchSeller = searchBar.text!
    }
    
}





