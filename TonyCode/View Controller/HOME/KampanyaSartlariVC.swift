//
//  KampanyaSartlariVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 23.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class KampanyaSartlariVC: UIViewController {

    @IBOutlet weak var lblBaslik: UILabel!
    @IBOutlet weak var textField: UITextView!
    
    @IBOutlet weak var HeaderTop: NSLayoutConstraint!
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11, *) {
            HeaderTop.constant = 60 + view.safeAreaInsets.top - self.navigationController!.navigationBar.frame.height - 20
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblBaslik.text = ""
        self.textField.text = ""
        
        if let seller = GlobalVariables.shared.selectedSellerDetail {
            self.lblBaslik.text = seller.name!
            self.textField.text = seller.sartlar!
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.textField.scrollRangeToVisible(NSMakeRange(0,0))
    }
    
}
