//
//  SellerDetail+Handlers.swift
//  TonyCode
//
//  Created by Yunus Tek on 21.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

extension SellerDetailVC: UIScrollViewDelegate {
    
    func fillScrollView()
    {
        self.scrollView.isPagingEnabled = true
        var countIndex = CGFloat(0)
        
        if let sliderList = GlobalVariables.shared.selectedSellerDetail?.photos {
            for list in sliderList {
                let image = UIImageView()
                image.frame = CGRect(x: countIndex * view.frame.width, y: 0, width: view.frame.width, height: self.scrollView.frame.height)
                if let imageUrl = list.photo {
                    DispatchQueue.main.async {
                        image.downloadedFrom(link: imageUrl)
                    }
                }

                countIndex += 1
                image.contentMode = .scaleAspectFill
                self.scrollView.addSubview(image)
            }
        }
        
        self.scrollView.contentSize = CGSize(width: CGFloat(countIndex) * view.bounds.size.width, height: scrollView.frame.height)
        
        // PageControl
        self.pageControl.numberOfPages = GlobalVariables.shared.selectedSellerDetail!.photos!.count
        if self.pageControl.numberOfPages <= 1 {
            self.pageControl.isHidden = true
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.backButton()
    }
    

}
