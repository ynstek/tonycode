//
//  SellerDetailVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 15.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class SellerDetailVC: UIViewController, UITextViewDelegate {

    @IBOutlet weak var tonyCodeView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var viewTopSpace: NSLayoutConstraint!
    
    @IBOutlet weak var viewNeredeWidth: NSLayoutConstraint!
    @IBOutlet weak var viewPaylasWidth: NSLayoutConstraint!

    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    @IBOutlet weak var txtViewDesc: UITextView!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblSure: UILabel!
    
    @IBOutlet weak var imgFav: UIButton!
    
    @IBOutlet weak var HeaderTop: NSLayoutConstraint!
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11, *) {
            HeaderTop.constant = 60 + view.safeAreaInsets.top - self.navigationController!.navigationBar.frame.height - 20
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setKonumlar()
        self.getSellerDetail()

        imgFav.setImage(UIImage(named: "notfav"), for: .normal)
        lblName.text = ""
        lblDiscount.text = "%0"
        lblSure.text = "Sona Erdi"
        imgFav.setImage(UIImage(named: "notfav"), for: .normal)
        
        if let customer = GlobalVariables.shared.currentCustomer {
            if customer.sure!.ToDate() >= Date() {
                lblSure.text = String(customer.sure!.ToDate().ToString())
            }
        }
        
        tonyCodeView.setGradientBackground(one: UIColor("#E41F1C") , two: UIColor("#AE0A08"))
        
    }
    
    @IBAction func btnTonyCode(_ sender: UIButton) {
        tabBarController?.selectedIndex = 2 // TonyCode
    }
    
    @IBAction func btnFavorite(_ sender: Any) {
        if Reachability.isConnectedToNetwork() {
            JsonFavorite.Todo.connect(seller_id: GlobalVariables.shared.selectedSellerId) { (result, error) in
                if result != nil {
                    DispatchQueue.main.async() {
                        if result! {
                            self.imgFav.setImage(UIImage(named: "fav"), for: .normal)
                        } else {
                            self.imgFav.setImage(UIImage(named: "notfav"), for: .normal)
                        }
                        GlobalFunctions.shared.animationShake(self.imgFav)
                    }
                }
            }
        }
    }
    
    @IBAction func btnPaylas(_ sender: Any) {
        let myWebsite = NSURL(string: "http://itunes.apple.com/app/" + "tonycode/1372282050") // app store id
        guard let url = myWebsite else {
            print("nothing found")
            return
        }
        
        let seller = GlobalVariables.shared.selectedSellerDetail!
        let text = seller.name!.uppercased() + " için TONYCODE'a özel %" + String(seller.discount!) + " indirim fırsatı var."
        
        let shareItems:Array = [text + "\n", url] as [Any]
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivityType.print, UIActivityType.postToWeibo, UIActivityType.copyToPasteboard, UIActivityType.addToReadingList, UIActivityType.postToVimeo]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func setKonumlar() {
        self.viewNeredeWidth.constant = (self.view.frame.size.width - 58) / 2
        self.viewPaylasWidth.constant = self.viewNeredeWidth.constant - 10
    }
    
    func getSellerDetail() {
        if Reachability.isConnectedToNetwork() {
            self.activityOpen()
            JsonSellerDetail.Todo.connect(id: GlobalVariables.shared.selectedSellerId) { (result, error) in
                self.activityClose()
                if result != nil {
                    GlobalVariables.shared.selectedSellerDetail = result!
                    DispatchQueue.main.async() {
                        self.lblName.text = result!.name!
                        self.txtViewDesc.text = result!.desc!
                        self.lblDiscount.text = "%" + String(result!.discount!)
                        
                        if result!.favorite != -1 {
                            self.imgFav.setImage(UIImage(named: "fav"), for: .normal)
                        } else {
                            self.imgFav.setImage(UIImage(named: "notfav"), for: .normal)
                        }
                        
                        self.setViewHeight()
                        self.fillScrollView()
                    }
                } else {
                    // goBack
                    _ = self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func setViewHeight() {
        let fixedWidth = txtViewDesc.frame.size.width
        txtViewDesc.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = txtViewDesc.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = txtViewDesc.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        txtViewDesc.frame = newFrame
        
        
        self.scrollViewHeight.constant = 654 + txtViewDesc.frame.size.height
        
    }
    
}
