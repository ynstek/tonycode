//
//  SellerMapVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 18.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit
import MapKit

class SellerMapVC: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblName2: UILabel!
    @IBOutlet weak var lblAdres: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblOpeningTime: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var requestCLLocation = CLLocation()
    
    @IBOutlet weak var lblDistance: UILabel!
    
    @IBOutlet weak var HeaderTop: NSLayoutConstraint!
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11, *) {
            HeaderTop.constant = 60 + view.safeAreaInsets.top - self.navigationController!.navigationBar.frame.height - 20
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        detailView.setGradientBackground(one: UIColor("#FFFFFF"), two: UIColor("D8D8D8"))
        fillSellerInfo()
    }
    
    func fillSellerInfo() {
        setMapLocation()
        if let seller = GlobalVariables.shared.selectedSellerDetail {
            lblName.text = seller.name
            lblName2.text = seller.name
            lblAdres.text = seller.adres
            lblPhone.text = seller.phone
            lblOpeningTime.text = seller.opening_date! + " - " + seller.closing_date!
            
            lblDistance.text = ""
            
            if let location = GlobalFunctions.shared.getCurrentLocation() {
                
                let coordinate1 = CLLocation(latitude: location.latitude!, longitude: location.longitude!)
                
                let coordinate2 = CLLocation(latitude: Double(seller.latitude!)!, longitude: Double(seller.longitude!)!)
                
                
                let distanceInMeters = coordinate1.distance(from: coordinate2) // result is in meters
                lblDistance.text = (distanceInMeters/1000).converToString() + " km"
            }
        }
    }

    // AnnotationImage
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        let annotationIdentifier = "Identifier"
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "pin2")
            
        }
        return annotationView
    }
    
    func setMapLocation() {
        if let seller = GlobalVariables.shared.selectedSellerDetail {
            var lat = Double(seller.latitude!)!
            var long = Double(seller.longitude!)!
            
            if lat == 0 && long == 0 {
                if let location = GlobalFunctions.shared.getCurrentLocation() {
                    lat = location.latitude!
                    long = location.longitude!
                }
            }
            
            let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
            
            let span = MKCoordinateSpan(latitudeDelta: 0.002, longitudeDelta: 0.002)
            let region = MKCoordinateRegion(center: location, span: span)
            mapView.setRegion(region, animated: true)
            
            setMapPin()
        } else {
            AlertFunctions.messageType.showOKAlert("Hata", bodyMessage: "Bir hata oluştu. Lütfen tekrar deneyiniz.")
        }
    }
    
    func setMapPin() {
        let seller = GlobalVariables.shared.selectedSellerDetail!
        
        var lat = Double(seller.latitude!)!
        var long = Double(seller.longitude!)!
        
        if lat == 0 && long == 0 {
            if let location = GlobalFunctions.shared.getCurrentLocation() {
                lat = location.latitude!
                long = location.longitude!
            }
        }
        
        let location = CLLocationCoordinate2D(latitude: lat, longitude: long)

        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = seller.name
//        annotation.subtitle = "Kampanya"
        self.mapView.addAnnotation(annotation)
    }
    
    @IBAction func btnYolTarifi(_ sender: Any) {
        self.yolTarifiAl()
    }
    
    func yolTarifiAl() {
        if let seller = GlobalVariables.shared.selectedSellerDetail {
            self.activityOpen()

            self.requestCLLocation = CLLocation(latitude: Double(seller.latitude!)!, longitude: Double(seller.longitude!)!)
            
            CLGeocoder().reverseGeocodeLocation(requestCLLocation) { (placemarks, error) in
                if let placemark = placemarks {
                    if placemark.count > 0 {
                        let newPlacemark = MKPlacemark(placemark: placemark[0])
                        let item = MKMapItem(placemark: newPlacemark)
                        item.name = seller.name
                        
                        let launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]
                        item.openInMaps(launchOptions: launchOptions)
                    }
                }
            }
            self.activityClose()
        } else {
            AlertFunctions.messageType.showOKAlert("Hata", bodyMessage: "Bir hata oluştu. Lütfen tekrar deneyiniz.")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
