//
//  ProfileCV.swift
//  TonyCode
//
//  Created by Yunus Tek on 12.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ProfileCV: UIViewController {
    
    @IBOutlet weak var txtBirthdate: UITextField!
    let birthdatePicker = UIDatePicker()
    @IBOutlet weak var txtCity: UITextField!
    let cityPicker = UIPickerView()
    @IBOutlet weak var txtGender: UITextField!
    let genderPicker = UIPickerView()
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var photoView: UIView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtLastname: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    var isChangePhoto = false

    var phoneNo = ""
    @IBOutlet weak var editImage: UIView!
    
    var isSozlesmeOnay = false
    @IBOutlet weak var btnSozlesme: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCustomer(phoneNo)

        imgProfile.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectProfileImage)))
        
        photoAnimate()
        createKeyboards()
    }
    
    @IBAction func btnSozlesme(_ sender: UIButton) {
        isSozlesmeOnay = !isSozlesmeOnay
        sender.backgroundColor = isSozlesmeOnay ? UIColor.red : UIColor.white
    }
    
    func photoAnimate(){
        GlobalFunctions.shared.animationShake(imgProfile)
        GlobalFunctions.shared.animationShake(photoView)
    }
    
    @IBAction func btnBilgileriOnayla(_ sender: UIButton) {
        if isValid() {
            updateCustomer()
        }
    }
    @IBAction func btnMusteriSozlesme(_ sender: Any) {
        self.openSozlesmeView()
    }
    
    fileprivate func openSozlesmeView() {
        let popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SozlesmeView") as! SozlesmeVC
        
        popover.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        popover.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        let popoverCont = popover.popoverPresentationController
        popoverCont?.permittedArrowDirections = .any
        //        popoverCont?.sourceView = sender as? UIView
        self.present(popover, animated: true, completion: nil)
    }
}

