//
//  LoginVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 8.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import AnyFormatKit

class LoginVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var btnDevam: UIButton!
    @IBOutlet weak var btnDevamBottom: NSLayoutConstraint!
    @IBOutlet weak var phoneView: UIView!
    
    var safeArea: CGFloat = 0

    // TextViewFormatter
//    var txtPhone = TextInputController()
//    var formatter: TextInputFormatter? = TextInputFormatter(textPattern: "###", prefix: "")
//    let txtPhoneField = TextInputField(frame: CGRect(x: 93, y: 11, width: 166, height: 29))
//    var PhoneNo = ""
    
    var keyboardHeight: CGFloat = -1
    
    // Ulke Kodu
    @IBOutlet weak var txtCountryCode: UITextField!
    let countryCodePicker = UIPickerView()
    
    @IBOutlet weak var txtPhoneNumber: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        keyboardCreate()
    }
    
    fileprivate func keyboardCreate() {
        createContryCodePicker()
//        createPhoneNumberField()

        
        // Klavyenin texte uzakligi
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 104
        // Bos yere tiklayinca klavyeyi kapat
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enableAutoToolbar = false

        NotificationCenter.default.addObserver(self, selector: #selector(self.openKeyboard), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.closeKeyboard), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        self.buttonFrame()
    }
    
    override func viewSafeAreaInsetsDidChange() {
        print("SAFEAREA viewSafeAreaInsetsDidChange")
    }
    
    func buttonFrame() {
        btnDevamBottom.constant = safeArea

        if keyboardHeight != -1 {
            btnDevamBottom.constant = 0
            btnDevamBottom.constant += keyboardHeight + self.view.frame.origin.y + safeArea
        }
    }
    
    @objc func openKeyboard(notification: NSNotification?) {
        if let keyboardSize = (notification!.userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            btnDevamBottom.constant += keyboardSize.height + self.view.frame.origin.y + safeArea + 10
            keyboardHeight = keyboardSize.height
        }
    }
    
    @objc func closeKeyboard(notification: NSNotification) {
        btnDevamBottom.constant = safeArea
        keyboardHeight = -1
        self.view.frame.origin.y = 0
    
//        self.txtPhone.setAndFormatText(PhoneNo)
    }

    @IBAction func btnDidBeginPhone(_ sender: UITextField) {

    }
    
    @IBAction func btnDevam(_ sender: UIButton) {
        let phoneNumber = self.txtCountryCode.text! + self.txtPhoneNumber.text!
        if GlobalFunctions.isValidPhone(phoneNumber) {
            if Reachability.isConnectedToNetwork() {
                self.activityOpen()
                JsonSms.Todo.sms(phone: phoneNumber, completionHandler: { (result, error) in
                    self.activityClose()
                    if result {
                        DispatchQueue.main.async() {
                            self.performSegue(withIdentifier: "showPhoneActivation", sender: nil)
                        }
                    }
                })
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //        if segue.identifier == "Details" {
        let vc = segue.destination as! PhoneActivationVC
        
        let phoneNumber = self.txtCountryCode.text! + self.txtPhoneNumber.text!
        vc.currentPhoneNo = phoneNumber //txtPhone.unformattedText()!
        //        }
    }
}

extension LoginVC { // CountryCode
    
//    func createPhoneNumberField() {
//        txtPhone = TextInputController()
//
//        if let format = GlobalVariables.shared.jsonCountryCode[countryCodePicker.selectedRow(inComponent: 0)].format {
//            formatter = TextInputFormatter(textPattern: format, prefix: "")
//            txtPhone.formatter = formatter
//        }
//
//        formatter?.allowedSymbolsRegex = "[0-9]"
//        txtPhone.textInput = txtPhoneField
//
//        // createTextField
//        txtCountryCode.placeholder = "Telefon No"
//        txtPhoneField.font = UIFont(name: "AvenirNext-Bold", size: 20)
//        txtCountryCode.minimumFontSize = 5
//        txtPhoneField.adjustsFontSizeToFitWidth = true
//        txtPhoneField.autocorrectionType = UITextAutocorrectionType.no
//        txtPhoneField.keyboardType = UIKeyboardType.numberPad
//        txtPhoneField.returnKeyType = UIReturnKeyType.done
//        txtPhoneField.borderStyle = .none
//        txtPhoneField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
//
//        // Add Field
//        txtPhoneField.textInputDelegates.add(delegate: self)
//        phoneView.addSubview(txtPhoneField)
//    }
    
    func createContryCodePicker() {
        countryCodePicker.delegate = self
        countryCodePicker.dataSource = self
        
        txtCountryCode.inputView = countryCodePicker
        txtCountryCode.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(btnDoneCountryCode))
        txtCountryCode.keyboardToolbar.nextBarButton.setTarget(self, action: #selector(btnDoneCountryCode))
        txtCountryCode.keyboardToolbar.previousBarButton.setTarget(self, action: #selector(btnDoneCountryCode))
    }
    
    @objc func btnDoneCountryCode() {
        txtCountryCode.text = GlobalVariables.shared.jsonCountryCode[countryCodePicker.selectedRow(inComponent: 0)].tel

//        createPhoneNumberField()
    }
    
    // MARK:- PickerView Delegate & DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return GlobalVariables.shared.jsonCountryCode.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let data = GlobalVariables.shared.jsonCountryCode[row]
        
        return data.code! + " (" + data.tel! + ")"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.txtCountryCode.text = GlobalVariables.shared.jsonCountryCode[row].tel

    }
}

//extension LoginVC: TextInputDelegate {
//    func textInputDidBeginEditing(_ textInput: TextInput) {
//    }
//    func textInputShouldBeginEditing(_ textInput: TextInput) -> Bool {
//        return true
//    }
//    func textInput(_ textInput: TextInput, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        DispatchQueue.main.async() {
//
//            if let no = self.txtPhone.unformattedText() {
//                self.PhoneNo = no
//            } else {
////                self.PhoneNo = self.txtPhoneField.content!
//            }
//
//            let color = self.PhoneNo.count > 1 ? "#E52320" : "#95989A"
//            self.btnDevam.backgroundColor = UIColor(color)
//            print("SAVE")
//
//        }
//        return true
//    }
//}
