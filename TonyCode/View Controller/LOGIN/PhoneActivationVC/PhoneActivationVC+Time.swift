//
//  TimerCountDown.swift
//  TonyCode
//
//  Created by Yunus Tek on 24.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

extension PhoneActivationVC {
    
    func runProgressRing() {
        seconds = maxSecond
        self.btnResendCode.isEnabled = false
        self.btnResendCode.setTitleColor(UIColor("#95989A"), for: .normal)
        timer.invalidate()
        
        //
        let center = CGPoint(x: progressRingView.frame.width / 2, y: progressRingView.frame.height / 2)
        let circularPath = UIBezierPath(arcCenter: center, radius: 30, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
        
        // Create my track layer
        let trackLater = CAShapeLayer()
        trackLater.path = circularPath.cgPath
        trackLater.strokeColor = UIColor.black.cgColor
        trackLater.lineWidth = 5
        trackLater.fillColor = UIColor.clear.cgColor
        trackLater.lineCap = kCALineCapRound
        progressRingView.layer.addSublayer(trackLater)
        
        // Create my shape layer
        shapeLayer.path = circularPath.cgPath
        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.lineWidth = 5
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = kCALineCapRound
        shapeLayer.strokeEnd = 0 //
        progressRingView.layer.addSublayer(shapeLayer)
        
        // Start Timer
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(PhoneActivationVC.runTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func runTimer() {
        seconds -= 1
        self.lblTime.text = String(seconds)
        self.shapeLayer.strokeEnd += CGFloat(1) / CGFloat(76.5)
        
        if seconds == 0 {
            self.shapeLayer.strokeEnd = 1
            timer.invalidate()
            seconds = maxSecond
            self.btnResendCode.isEnabled = true
            self.btnResendCode.setTitleColor(UIColor.blue, for: .normal)
        }
    }
    
}
