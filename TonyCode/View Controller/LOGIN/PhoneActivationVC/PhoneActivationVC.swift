//
//  PhoneActivationVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 7.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import AnyFormatKit

class PhoneActivationVC: UIViewController {

    @IBOutlet weak var progressRingView: UIView!
    @IBOutlet weak var lblPhoneNo: UILabel!
    var currentPhoneNo = ""
    
    // TIMER
    @IBOutlet weak var lblTime: UILabel!
    let shapeLayer = CAShapeLayer()
    var seconds = 60
    var timer = Timer()
    let maxSecond = 60
    
    @IBOutlet weak var btnResendCode: UIButton!
    @IBOutlet weak var btnDevam: UIButton!
    @IBOutlet weak var btnDevamBottom: NSLayoutConstraint!
    var safeArea: CGFloat = 0

    @IBOutlet weak var codeView: UIView!
    
    // TextViewFormatter
    let txtCode = TextInputController()
    let formatter = TextInputFormatter(textPattern: "# # # # # #", prefix: "")
    let txtCodeField = TextInputField(frame: CGRect(x: 28, y: 7, width: 210, height: 39))
    var CodeNo = ""

    var keyboardHeight: CGFloat = -1
    
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11, *) {
            //            safeArea = view.safeAreaInsets.bottom
        }
    }
    
    var isSmsControl = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblPhoneNo.text = currentPhoneNo
        runProgressRing()
        createCodeField()
        
        keyboardCreate()
    }
    
    override func viewWillLayoutSubviews() {
        self.buttonFrame()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnResendCode(_ sender: Any) {
        self.runProgressRing()
        if Reachability.isConnectedToNetwork() {
            self.activityOpen()
            JsonSms.Todo.sms(phone: currentPhoneNo, completionHandler: { (result, error) in
                self.activityClose()
                if result {
                    
                } else {
                    AlertFunctions.messageType.showOKAlert("Sms Gönderilemedi", bodyMessage: "Bir hata oluştu lütfen tekrar deneyiniz.")
                }
            })
        }
    }

    fileprivate func keyboardCreate() {
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 64 // 54 + 10
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(PhoneActivationVC.openKeyboard), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PhoneActivationVC.closeKeyboard), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }
    
    @IBAction func btnDevam(_ sender: UIButton?) {
        if txtCode.unformattedText()!.count == 6 {
            if isSmsControl == false {
            if Reachability.isConnectedToNetwork() {
                self.activityOpen()
                self.isSmsControl = true
                JsonSms.Todo.control(phone: currentPhoneNo, code: self.txtCode.unformattedText()!, completionHandler: { (result, error) in
                    self.activityClose()
                    
                    if result!.result!.lowercased() == "nouser" {
                        DispatchQueue.main.async() {
                            self.performSegue(withIdentifier: "showPhoneActivation", sender: nil)
                        }
                    } else if result!.result!.lowercased() == "error" {
                        DispatchQueue.main.async() {
                            self.isSmsControl = false
                            AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: result!.error_message!)
                        }
                    } else { // Success
                        GlobalVariables.shared.currentCustomer = result!.response!
                        // Kaydet
                        UserDefaults.standard.set(GlobalVariables.shared.currentCustomer!.id!, forKey: "id")
                        UserDefaults.standard.synchronize()
                        DispatchQueue.main.async() {
                            GlobalFunctions.shared.firebaseSetTokenAndTopic()
                            self.performSegue(withIdentifier: "home", sender: nil)
                        }
                    }
                    
                })
            }
            }
        } else {
            AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Doğrulama kodu 6 basamaklı olmalıdır. Lütfen tekrar deneyiniz.")
        }
    }
    
    
    func createCodeField() {

        formatter.allowedSymbolsRegex = "[0-9]"
        txtCode.textInput = txtCodeField
        txtCode.formatter = formatter
        
        // createTextField
        txtCodeField.font = UIFont(name: "AvenirNext-Bold", size: 25)
        txtCodeField.autocorrectionType = UITextAutocorrectionType.no
        txtCodeField.keyboardType = UIKeyboardType.numberPad
        txtCodeField.returnKeyType = UIReturnKeyType.done
        txtCodeField.borderStyle = .none
        txtCodeField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        txtCodeField.textAlignment = .center
        
        // Add Field
        txtCodeField.textInputDelegates.add(delegate: self)

        codeView.addSubview(txtCodeField)
    }
    
    func buttonFrame() {
        if #available(iOS 11, *) {
//            safeArea = view.safeAreaInsets.bottom
        }
        btnDevamBottom.constant = safeArea
        
        if keyboardHeight != -1 {
            btnDevamBottom.constant = 0
            btnDevamBottom.constant += keyboardHeight + self.view.frame.origin.y + safeArea
        }
    }

    @objc func openKeyboard(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            btnDevamBottom.constant += keyboardSize.height + self.view.frame.origin.y + safeArea
            keyboardHeight = keyboardSize.height
        }
    }
    
    @objc func closeKeyboard(notification: NSNotification) {
        btnDevamBottom.constant = safeArea
        keyboardHeight = -1
        self.view.frame.origin.y = 0

        self.txtCode.setAndFormatText(CodeNo)
    }

    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//        NotificationCenter.default.removeObserver(self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPhoneActivation" {
            let vc = segue.destination as! ProfileCV
            vc.phoneNo = lblPhoneNo.text!
        }
        
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 10
    }
    
    func autoAction() {
        if txtCode.unformattedText()!.count == 6 {
            if isSmsControl == false {
                if Reachability.isConnectedToNetwork(false) {
                    self.isSmsControl = true
                    JsonSms.Todo.control(phone: currentPhoneNo, code: self.txtCode.unformattedText()!, completionHandler: { (result, error) in
                        
                        if result!.result!.lowercased() == "nouser" {
                            DispatchQueue.main.async() {
                                self.performSegue(withIdentifier: "showPhoneActivation", sender: nil)
                            }
                        } else if result!.result!.lowercased() == "error" {
                            self.isSmsControl = false
                        } else { // Success
                            GlobalVariables.shared.currentCustomer = result!.response!
                            // Kaydet
                            UserDefaults.standard.set(GlobalVariables.shared.currentCustomer!.id!, forKey: "id")
                            UserDefaults.standard.synchronize()
                            DispatchQueue.main.async() {
                                self.performSegue(withIdentifier: "home", sender: nil)
                            }
                        }
                        
                    })
                }
            }
        }
    }
    
}

extension PhoneActivationVC: TextInputDelegate {
    func textInputDidBeginEditing(_ textInput: TextInput) {
    }
    func textInputShouldBeginEditing(_ textInput: TextInput) -> Bool {
        return true
    }
    func textInput(_ textInput: TextInput, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        DispatchQueue.main.async() {
            self.CodeNo = self.txtCode.unformattedText()!

            let color = self.txtCode.unformattedText()!.count == 6 ? "#E52320" : "#95989A"
            self.btnDevam.backgroundColor = UIColor(color)
            if self.txtCode.unformattedText()!.count == 6 {
                self.autoAction()
            }
        }

        return true
    }
}
