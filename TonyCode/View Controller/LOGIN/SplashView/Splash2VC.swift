//
//  Splash2VC.swift
//  TonyCode
//
//  Created by Yunus Tek on 15.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class Splash2VC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        GlobalFunctions.shared.getLocation(false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnRegisterOrLogin(_ sender: UIButton) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: "Login")
        
        present(presentedVC, animated: false, completion: nil)
    }

}
