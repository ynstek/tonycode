//
//  SplashVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 15.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit
import Firebase

class SplashVC: UIViewController {

    @IBOutlet weak var openView: UIView!
    
    var isCustomer: Bool = false
    
    var successCount = 2
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateData()
    }

    fileprivate func updateData() {
        self.activityOpen()
        openView.isHidden = true

        if Reachability.isConnectedToNetwork() {
            // 1 Customer
            if let id = UserDefaults.standard.value(forKey: "id") as? Int {
                print("current id:", id)
                JsonGetCustomer.Todo.connect(id: id, completionHandler: { (result, error) in
                    if result != nil {
                        self.isCustomer = true
                        self.open()
                    } else {
                        self.open()
                    }
                })
            } else {
                // Country Code
                JsonCountryCode.Todo.connect(completionHandler: { (result, error) in
                    self.open()
                })
            }
            
            // 2 CityData
            if let data = UserDefaults.standard.value(forKey: "jsonCity") as? Data {
                GlobalVariables.shared.jsonCity = try! PropertyListDecoder().decode([JsonCity.Response].self, from: data)
                self.open()
            } else {
                JsonCity.Todo.connect(completionHandler: { (result, error) in
                    self.open()
                })
            }
        } else {
            self.activityClose()
        }
    }
    
    func open() {
        self.count+=1
        print("Download: ", count, "/", successCount)
        if count == successCount {
            DispatchQueue.main.async() {
                if GlobalVariables.shared.currentCustomer == nil {
                    self.activityClose()
                    self.openView.isHidden = false
                    GlobalFunctions.shared.animationShake(self.openView)
                    
                    self.btnAction(nil)
                } else if GlobalVariables.shared.currentCustomer != nil {
                    GlobalFunctions.shared.firebaseSetTokenAndTopic()
                    self.performSegue(withIdentifier: "home", sender: nil)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnAction(_ sender: UIButton?) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: "Splash2")

        present(presentedVC, animated: false, completion: nil)
    }

}
