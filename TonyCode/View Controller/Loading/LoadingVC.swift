//
//  LoadingVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 27.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class LoadingVC: UIViewController {

    @IBOutlet weak var centerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async() {
            let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "loading", withExtension: "gif")!)
            let gif = UIImage.gifImageWithData(imageData!)
            
            //        let gifURL : String = "https://cdn.dribbble.com/users/600626/screenshots/2944614/loading_12.gif"
            //        let gif = UIImage.gifImageWithURL(gifURL)
            
            let imageView = UIImageView(image: gif)
            imageView.frame = CGRect(x: 0, y: 0, width: self.centerView.frame.width, height: self.centerView.frame.height)
            self.centerView.addSubview(imageView)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
