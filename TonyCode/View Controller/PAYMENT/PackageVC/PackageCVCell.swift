//
//  PackageCVCell.swift
//  TonyCode
//
//  Created by Yunus Tek on 1.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class PackageCVCell: UICollectionViewCell {
    
    var selectedItem: Bool = false {
        didSet {
            cellEditable(selectedItem)
        }
    }
    
    func cellEditable(_ status: Bool){
        if status {
            self.time!.textColor = UIColor.white
            self.price.textColor = UIColor.white
            self.seperator.textColor = UIColor.white
            
            self.viewback.backgroundColor = UIColor.black
        } else {
            self.time!.textColor = UIColor.black
            self.price.textColor = UIColor.black
            self.seperator.textColor = UIColor.black
            
            self.viewback.backgroundColor = UIColor.white
        }
        
        selectIcon.isHidden = !status
    }
    
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var seperator: UILabel!
    @IBOutlet weak var selectIcon: UIImageView!
    @IBOutlet weak var viewback: UIView!
    
}
