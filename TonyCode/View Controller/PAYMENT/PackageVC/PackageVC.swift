//
//  PackageVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 1.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class PackageVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    lazy var jsonPackages : [JsonPackage.Response] = {
        return [JsonPackage.Response]()
    }()
    
    var selectedPackage: JsonPackage.Response? = nil
    
    @IBOutlet weak var HeaderTop: NSLayoutConstraint!
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11, *) {
            HeaderTop.constant = 60 + view.safeAreaInsets.top - self.navigationController!.navigationBar.frame.height - 20
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getPackages()
        self.presentTransparentNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getPackages() {
        if Reachability.isConnectedToNetwork() {
            self.activityOpen()
            JsonPackage.Todo.connect(completionHandler: { (result, error) in
                self.activityClose()
                if result != nil {
                    DispatchQueue.main.async() {
                        self.jsonPackages = result!
                        self.selectedPackage = result![0]
                        self.collectionView.reloadData()
                    }
                }
            })
        }
    }
}


extension PackageVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    // MARK: CollectionView
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.width - 50) / 2, height: 125)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "reuse", for: indexPath)
            
            return headerView
        default:
            //                        assert(false, "Unexpected element kind")
            fatalError("Unexpected element kind")
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return jsonPackages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PackageCVCell
        
        let data = jsonPackages[indexPath.row]
        
        cell.selectedItem = data.id == selectedPackage!.id! ? true : false
        cell.time.text = data.time
        cell.price.text = data.price! + " TL"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedPackage = jsonPackages[indexPath.row]
        self.collectionView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //if segue.identifier == "Details" {
        let vc = segue.destination as! PayVC
        vc.selectedPackage = self.selectedPackage
        self.backButton()
    }
    
}

