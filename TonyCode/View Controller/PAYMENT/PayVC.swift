//
//  PayVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 1.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import AnyFormatKit

class PayVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var btnOde: UIButton!
    var selectedPackage: JsonPackage.Response? = nil
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var txtHoldername: UITextField!
    
    @IBOutlet weak var txtMonth: UITextField!
    let monthPicker = UIPickerView()
    var months = [Int]()
    
    @IBOutlet weak var txtYear: UITextField!
    let yearPicker = UIPickerView()
    var years = [Int]()
    
    // CardNumber TextViewFormatter
    let txtCardNumberInput = TextInputController()
    let cardNumberFormatter = TextInputFormatter(textPattern: "#### #### #### ####", prefix: "")
    let cardNumberField = TextInputField(frame: CGRect(x: 16, y: 12, width: 360 , height: 20))
    @IBOutlet weak var cardNumverView: UIView!
    var txtCardNumber = ""
    
    // Cvc TextViewFormatter
    let txtCvcInput = TextInputController()
    let cvcFormatter = TextInputFormatter(textPattern: "###", prefix: "")
    let cvcField = TextInputField(frame: CGRect(x: 16, y: 12, width: 360, height: 20))
    @IBOutlet weak var cvcView: UIView!
    var txtCvc = ""
    
    @IBOutlet weak var HeaderTop: NSLayoutConstraint!
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11, *) {
            HeaderTop.constant = 60 + view.safeAreaInsets.top - self.navigationController!.navigationBar.frame.height - 20
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Package ID", selectedPackage!.id!)
        time.text = selectedPackage!.time!
        price.text = selectedPackage!.price! + " TL"

        createKeyboards()
    }
    
    @objc func goBack(_ notification: Notification) {
        // goBack
        NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadSeller"), object: nil)
        
        _ = navigationController?.popViewController(animated: true)
        tabBarController?.selectedIndex = 0 // Home
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
    }
    
    fileprivate func openSuccessView() {
        let popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "successPopover") as! SuccessPaymentVC
        
        popover.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        popover.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        let popoverCont = popover.popoverPresentationController
        popoverCont?.permittedArrowDirections = .any
        //        popoverCont?.sourceView = sender as? UIView
        self.present(popover, animated: true, completion: nil)
    }
    
    fileprivate func openErrorView() {
        let popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "errorPopover") as! ErrorPaymentVC
        
        popover.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        popover.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        let popoverCont = popover.popoverPresentationController
        popoverCont?.permittedArrowDirections = .any
        //        popoverCont?.sourceView = sender as? UIView
        self.present(popover, animated: true, completion: nil)
    }
    
    @objc func openResultPopover(_ notification: Notification) {
        if let success = GlobalVariables.shared.cardInfo?.success {
            if success {
                NotificationCenter.default.addObserver(self, selector: #selector(self.goBack(_:)), name: NSNotification.Name(rawValue: "goBack"), object: nil)

                JsonGetCustomer.Todo.connect(id: GlobalVariables.shared.currentCustomer!.id!, completionHandler: { (result, error) in
                })
                
                self.openSuccessView()
                
            } else {
                openErrorView()
            }
            GlobalVariables.shared.cardInfo!.success = nil
        }
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
    }
    
    func createKeyboards() {
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 20
    
        createCardNumberField()
        createCvcField()
        createMonthPicker()
        createYearPicker()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.closeKeyboard), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }
    
    @objc func closeKeyboard(notification: NSNotification) {
        print("CVC",txtCvcInput.unformattedText()!,txtCvc)
        print("CARD",txtCardNumberInput.unformattedText()!,txtCardNumber)

        self.txtCvcInput.setAndFormatText(txtCvc)
        self.txtCardNumberInput.setAndFormatText(txtCardNumber)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    fileprivate func openPaymentControl() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.openResultPopover(_:)), name: NSNotification.Name(rawValue: "openResultPopover"), object: nil)

        
        let PaymentConfirm = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentConfirm") as! PaymentConfirmVC
        PaymentConfirm.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        PaymentConfirm.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        let popoverCont = PaymentConfirm.popoverPresentationController
        popoverCont?.permittedArrowDirections = .any
        //        popoverCont?.sourceView = sender as? UIView
        self.present(PaymentConfirm, animated: true, completion: nil)
    }
    
    @IBAction func btnOde(_ sender: Any) {
        if isValid() {
            self.cardControl()
        }
    }
    
    func cardControl() {
        if Reachability.isConnectedToNetwork() {
            self.activityOpen()
            GlobalVariables.shared.cardInfo = JsonPay.CardInfo(packageid: self.selectedPackage!.id!
                ,holdername: self.txtHoldername.text!
                ,cardnumber: self.txtCardNumberInput.unformattedText()!
                ,month: self.txtMonth.text!
                ,year: self.txtYear.text!
                ,cvc: self.txtCvcInput.unformattedText()!
                ,htmlCode: "", errorMessage: "", success: nil
            )
            JsonPay.Todo.odeme(completionHandler: { (result, error) in
                self.activityClose()
                if result != nil {
                    if result!.result?.lowercased() == "success" {
                        DispatchQueue.main.async() {
                            GlobalVariables.shared.cardInfo!.htmlCode = result!.response!
                            
                            self.openPaymentControl()
                        }
                    } else {
                        DispatchQueue.main.async() {
                            AlertFunctions.messageType.showOKAlert("Ödeme Başarısız", bodyMessage: result!.response!)
                        }
                    }
                } else {
                    DispatchQueue.main.async() {
                        AlertFunctions.messageType.showOKAlert("Ödeme Başarısız", bodyMessage: "Bir hata oluştu. Lütfen tekrar deneyiniz.")
                    }
                }
            })
        }
    }
    
    func isValid(_ error: Bool = true) -> Bool {
        if txtHoldername.text!.count > 3
            &&  txtHoldername.text!.range(of: " ") != nil
        && txtCardNumberInput.unformattedText()!.count == 16
            && !txtMonth.text!.isEmpty
            && !txtYear.text!.isEmpty
            && txtCvcInput.unformattedText()!.count == 3
        {
            return true
        } else {
            if error {
                AlertFunctions.messageType.showOKAlert("Kart bilgileri eksik", bodyMessage: "Lütfen bilgileri kontrol edip tekrar deneyiniz.")
            }
            return false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let vc = segue.destination as! PhoneActivationVC
//        vc.currentPhoneNo = txtPhone.unformattedText()!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension PayVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func getToday() -> DateComponents {
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        
        return components
    }
    
    func createMonthPicker() {
        
        for i in 1...12 {
            months.append(i)
        }
        
        self.monthPicker.delegate = self
        self.monthPicker.dataSource = self
        
        txtMonth.inputView = monthPicker
        let selector = #selector(btnDoneMonth)
        txtMonth.keyboardToolbar.doneBarButton.setTarget(self, action: selector)
        txtMonth.keyboardToolbar.nextBarButton.setTarget(self, action: selector)
        txtMonth.keyboardToolbar.previousBarButton.setTarget(self, action: selector)
    }
    
    @objc func btnDoneMonth() {
        txtMonth.text = pickerView(self.monthPicker, titleForRow: monthPicker.selectedRow(inComponent: 0), forComponent: 0)
    }
    
    func createYearPicker() {
        
        for i in getToday().year!...2050 {
            years.append(i)
        }
        
        self.yearPicker.delegate = self
        self.yearPicker.dataSource = self
        
        txtYear.inputView = yearPicker

        let selector = #selector(btnDoneYear)
        txtYear.keyboardToolbar.doneBarButton.setTarget(self, action: selector)
        txtYear.keyboardToolbar.nextBarButton.setTarget(self, action: selector)
        txtYear.keyboardToolbar.previousBarButton.setTarget(self, action: selector)
    }
    
    @objc func btnDoneYear() {
        txtYear.text = pickerView(self.yearPicker, titleForRow: yearPicker.selectedRow(inComponent: 0), forComponent: 0)
    }
    
    // MARK:- PickerView Delegate & DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == monthPicker {
            return months.count
        } else if pickerView == yearPicker {
            return years.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == monthPicker {
            return String(months[row])
        } else if pickerView == yearPicker {
            return String(years[row])
        }
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }
}

extension PayVC: TextInputDelegate {
    func createCvcField() {
        cvcFormatter.allowedSymbolsRegex = "[0-9]"
        txtCvcInput.textInput = cvcField
        txtCvcInput.formatter = cvcFormatter
        
        // createTextField
        cvcField.font = UIFont(name: "AvenirNext-Regular", size: 20)
        cvcField.autocorrectionType = UITextAutocorrectionType.no
        cvcField.keyboardType = UIKeyboardType.numberPad
        cvcField.returnKeyType = UIReturnKeyType.done
        cvcField.borderStyle = .none
        cvcField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        cvcField.placeholder = "000"
        
        // Add Field
        cvcField.textInputDelegates.add(delegate: self)
        cvcView.addSubview(cvcField)
    }
    
    func createCardNumberField() {
        cardNumberFormatter.allowedSymbolsRegex = "[0-9]"
        txtCardNumberInput.textInput = cardNumberField
        txtCardNumberInput.formatter = cardNumberFormatter
        
        // createTextField
        cardNumberField.font = UIFont(name: "AvenirNext-Regular", size: 20)
        cardNumberField.autocorrectionType = UITextAutocorrectionType.no
        cardNumberField.keyboardType = UIKeyboardType.numberPad
        cardNumberField.returnKeyType = UIReturnKeyType.done
        cardNumberField.borderStyle = .none
        cardNumberField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        cardNumberField.placeholder = "0000 0000 0000 0000"
        
        // Add Field
        cardNumberField.textInputDelegates.add(delegate: self)
        cardNumverView.addSubview(cardNumberField)
    }
    
    func textInputDidBeginEditing(_ textInput: TextInput) {
    }
    func textInputShouldBeginEditing(_ textInput: TextInput) -> Bool {
        return true
    }
    func textInput(_ textInput: TextInput, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        DispatchQueue.main.async() {
            self.txtCvc = self.txtCvcInput.unformattedText()!
            self.txtCardNumber = self.txtCardNumberInput.unformattedText()!
            let color = self.isValid(false) ? "#E52320" : "#95989A"
            self.btnOde.backgroundColor = UIColor(color)
        }
        return true
    }

}
