//
//  ErrorPaymentVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 1.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class ErrorPaymentVC: UIViewController {

    @IBOutlet weak var errorMessage: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        errorMessage.text = GlobalVariables.shared.cardInfo!.errorMessage!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func brnClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
