//
//  PayVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 28.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit
import WebKit
import IQKeyboardManagerSwift

class PaymentConfirmVC: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityOpen()
        
        self.webView.loadHTMLString(GlobalVariables.shared.cardInfo!.htmlCode!, baseURL: nil)
//        webView.scrollView.isScrollEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print("WebView shouldStartLoadWith")
        if (navigationType == UIWebViewNavigationType.formSubmitted) {
            print("SUBMIT", request.url!.absoluteString)
            
            if request.url?.absoluteString == "https://tonycode.net/api/odeme/callback" {
                self.activityOpen()
                
//                requestBody = request.httpBody
//                threeparseData = webView.stringByEvaluatingJavaScript(from: "document.documentElement.outerHTML.toString()")!
                
                webView.isHidden = true
            }
            
            // https://sandbox-api.iyzipay.com/payment/mock/init3ds
            // https://sandbox-api.iyzipay.com/payment/mock/confirm3ds
            // https://sandbox-api.iyzipay.com/payment/iyzipos/callback3ds/success/3 // failure
            // https://tonycode.net/api/odeme/callback
        }
        return true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("WebView didFailLoadWithError")
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        print("WebView DidStartLoad")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityClose() // ilk acilis icin
        
        if webView.isHidden {
            if Reachability.isConnectedToNetwork() {
                self.activityOpen()
                let controlHtml: String = webView.stringByEvaluatingJavaScript(from: "document.documentElement.outerHTML.toString()")!
                
                var result = ""
                var response = ""
                
                var start = "result\":\""
                var end = "\""
                if let match = controlHtml.range(of: "(?<=\(start))[^\(end)]+", options: .regularExpression) {
                    result = String(controlHtml[match]).replaceTr()
                }
                
                start = "response\":\""
                end = "\""
                if let match = controlHtml.range(of: "(?<=\(start))[^\(end)]+", options: .regularExpression) {
                    response = String(controlHtml[match]).replaceTr()
                }
                
                if result.lowercased() == "success" {
                    GlobalVariables.shared.cardInfo!.success = true
                    GlobalVariables.shared.cardInfo!.errorMessage = response
                } else {
                    GlobalVariables.shared.cardInfo!.success = false
                    GlobalVariables.shared.cardInfo!.errorMessage = response
                }
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        print("WebView DidFinishLoad")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "openResultPopover"), object: nil)
    }
    
}

