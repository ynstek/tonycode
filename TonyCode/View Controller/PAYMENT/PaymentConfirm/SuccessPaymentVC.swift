//
//  SuccessPaymentVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 1.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class SuccessPaymentVC: UIViewController {
    
    @IBOutlet weak var errorMessage: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        errorMessage.text = "Ödeme Başarılı\n" + GlobalVariables.shared.cardInfo!.errorMessage!
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func brnClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "goBack"), object: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
