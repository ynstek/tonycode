//
//  BildirimDetayVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 27.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class BildirimDetayVC: UIViewController {

    @IBOutlet weak var baslik: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var HeaderTop: NSLayoutConstraint!
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11, *) {
            HeaderTop.constant = 60 + view.safeAreaInsets.top - self.navigationController!.navigationBar.frame.height - 20
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let data = GlobalVariables.shared.selectedBildirim!.message!
        baslik.text = data.title!
        textView.text = data.subtitle! + "\n\n" + data.body!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
