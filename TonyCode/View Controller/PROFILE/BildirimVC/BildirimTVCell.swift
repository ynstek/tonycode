//
//  BildirimTVCell.swift
//  TonyCode
//
//  Created by Yunus Tek on 27.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class BildirimTVCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
