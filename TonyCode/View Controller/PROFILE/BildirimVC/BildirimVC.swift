//
//  BildirimVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 27.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class BildirimVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var HeaderTop: NSLayoutConstraint!
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11, *) {
            HeaderTop.constant = 60 + view.safeAreaInsets.top - self.navigationController!.navigationBar.frame.height - 20
        }
    }
    
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshControlData), for: .valueChanged)
        return refreshControl
    }()
    
    @objc func refreshControlData() {
        // FIXME: Data refresh eklenecek
        getBildirimList(false)
        refreshControl.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getBildirimList()
        self.tableView.insertSubview(self.refreshControl, at: 0)
    }

    override func viewWillAppear(_ animated: Bool) {
        if AppDelegate.isNofiticationBildirim {
            AppDelegate.isNofiticationBildirim = false
//            performSegue(withIdentifier: "BildirimlerDetay", sender: nil)
        }
    }

    func getBildirimList(_ activity: Bool = true) {
        if Reachability.isConnectedToNetwork() {
            if activity {
                self.activityOpen()
            }
            JsonBildirim.Todo.connect(completionHandler: { (result, error) in
                self.activityClose()
                if result != nil {
                    GlobalVariables.shared.jsonBildirim = result!
                    DispatchQueue.main.async() {
                        self.tableView.reloadData()
                    }
                }
            })
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = GlobalVariables.shared.jsonBildirim.count
        
        tableView.isHidden = count == 0 ? true : false // For Bosview
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BildirimTVCell
        let data = GlobalVariables.shared.jsonBildirim[indexPath.row].message!
        
        cell.title.text = data.title!
        cell.body.text = data.subtitle! + "\n" + data.body!
        cell.date.text = ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        GlobalVariables.shared.selectedBildirim = GlobalVariables.shared.jsonBildirim[indexPath.row]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
