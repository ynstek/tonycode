//
//  FavorilerCVCell.swift
//  TonyCode
//
//  Created by Yunus Tek on 27.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class FavorilerCVCell: UICollectionViewCell {
 
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var name: UITextView!
    @IBOutlet weak var cat_icon: UIImageView!
    @IBOutlet weak var discount: UILabel!
    
    
}
