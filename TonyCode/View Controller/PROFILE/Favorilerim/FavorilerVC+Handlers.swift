//
//  FavorilerVC+Handlers.swift
//  TonyCode
//
//  Created by Yunus Tek on 27.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

extension FavorilerVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    // MARK: CollectionView
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.width - 20), height: 170)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let count = GlobalVariables.shared.jsonFavoriteList.count
        
        collectionView.isHidden = count == 0 ? true : false
        
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: favoriCell, for: indexPath) as! FavorilerCVCell
            
            let data = GlobalVariables.shared.jsonFavoriteList[indexPath.row]
            cell.name.text = data.name
            cell.photo.image = nil
            if let photo = data.photo {
                cell.photo.downloadedFrom(link: photo)
            }
            cell.cat_icon.image = nil
            if let icon = data.cat_icon {
                cell.cat_icon.downloadedFrom(link: icon)
            }

            cell.discount.text = "%" + String(data.discount!)
        
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        GlobalVariables.shared.selectedSeller = GlobalVariables.shared.jsonFavoriteList[indexPath.row]
        GlobalVariables.shared.selectedSellerId = GlobalVariables.shared.selectedSeller!.id!
        self.backButton()
    }
    
    // Son restoranda Yeni restoran ekle
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {        
        if indexPath.row == GlobalVariables.shared.jsonFavoriteList.count - 1 {
            loadNewSeller()
        }
    }
}
