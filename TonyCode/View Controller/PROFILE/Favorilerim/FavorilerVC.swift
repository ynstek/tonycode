//
//  FavorilerVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 27.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class FavorilerVC: UIViewController {

    @IBOutlet weak var favoriCV: UICollectionView!
    let favoriCell = "favoriCell"
    
    var selectedPageNo = 0
    var isExists = true
    
    @IBOutlet weak var bosiconTop: NSLayoutConstraint!
    @IBOutlet weak var bosTextCenter: NSLayoutConstraint!
    @IBOutlet weak var HeaderTop: NSLayoutConstraint!
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11, *) {
            HeaderTop.constant = 60 + view.safeAreaInsets.top - self.navigationController!.navigationBar.frame.height - 20
            bosTextCenter.constant = HeaderTop.constant
            bosiconTop.constant = HeaderTop.constant
        }
    }
    
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshControlData), for: .valueChanged)
        return refreshControl
    }()
    
    @objc func refreshControlData() {
        self.getSeller(false)
        refreshControl.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GlobalVariables.shared.jsonFavoriteList.removeAll()

        self.getSeller()
        self.favoriCV.insertSubview(self.refreshControl, at: 0)
    }

    func getSeller(_ activity: Bool = true) {
        // POST JSON
        selectedPageNo = 0
        isExists = true
        if Reachability.isConnectedToNetwork() {
            if activity {
                self.activityOpen()
            }
            JsonSeller.Todo.favorite(pageno: selectedPageNo,completionHandler: { (result, error) in
                self.activityClose()
                if result != nil {
                    DispatchQueue.main.async() {
                        GlobalVariables.shared.jsonFavoriteList = result!
                        if result!.count != GlobalVariables.shared.listLimit {
                            self.isExists = false
                        }
                        self.favoriCV.reloadData()
                    }
                }
            })
    
        }
    }
    
    func loadNewSeller () {
        if isExists {
            selectedPageNo += 1
            if Reachability.isConnectedToNetwork() {
                self.activityOpen()
                JsonSeller.Todo.favorite(pageno: selectedPageNo,completionHandler: { (result, error) in
                    self.activityClose()
                    if result != nil {
                        DispatchQueue.main.async() {
                            GlobalVariables.shared.jsonFavoriteList += result!
                            if result!.count != GlobalVariables.shared.listLimit {
                                self.isExists = false
                            }
                            self.favoriCV.reloadData()
                        }
                    }
                })
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.backButton()
    }

}
