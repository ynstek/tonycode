//
//  KisiselBilgilerimVC+Handlers.swift
//  TonyCode
//
//  Created by Yunus Tek on 25.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

extension KisiselBilgilerimVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    func isValid () -> Bool {
        txtEmail.text = txtEmail.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        if !txtName.text!.isEmpty && !txtLastname.text!.isEmpty && !txtBirthdate.text!.isEmpty && !txtCity.text!.isEmpty && !txtGender.text!.isEmpty && !txtEmail.text!.isEmpty
            && (txtTcNo.text!.isEmpty || txtTcNo.text!.count == 11)
        {
            
            if GlobalFunctions.isValidEmail(txtEmail.text!) {
                return true
            }
        } else {
            AlertFunctions.messageType.showOKAlert("Hatalı Giriş", bodyMessage: "Eksik veya hatalı bilgi girdiniz. Lütfen bilgileri kontrol edip tekrar deneyiniz.")
        }
        
        return false
    }
    
    func updateCustomer() {
        // POST JSON
        if Reachability.isConnectedToNetwork() {
            self.activityOpen()
            
            var imgString: String? = UIImagePNGRepresentation(imgProfile.image!.resizeUI(size: CGSize(width: 500, height: 500))!)!.base64EncodedString()
            imgString = isChangePhoto ? imgString : nil
            
            JsonUpdateCustomer.Todo.connect(name: txtName.text!, lastname: txtLastname.text!, email: txtEmail.text!, city: txtCity.text!, gender: txtGender.text!, birthdate: txtBirthdate.text!, profil_photo: imgString, tc: txtTcNo.text!, completionHandler: { (result, error, warning) in
                
                self.activityClose()
                
                if result != nil {
                    if result! {
                        DispatchQueue.main.async() {
                            AlertFunctions.messageType.showOKAlertVoid("Başarılı", bodyMessage: "Bilgilerin başarılı bir şekilde kaydedildi.", {
                                // goBack
                                _ = self.navigationController?.popViewController(animated: true)
                            })
                        }
                    } else if result! == false {
                        DispatchQueue.main.async() {
                            AlertFunctions.messageType.showOKAlert("Bir hata oluştu", bodyMessage: warning!)
                        }
                    }
                } else if error != nil {
                    DispatchQueue.main.async() {
                        AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error!.localizedDescription)
                    }
                }
            })
            
        }
    }
    
    func createKeyboards() {
        // Klavyenin texte uzakligi
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        createGenderPicker()
        createCityPicker()
        createDatePicker()
        
        imgProfile.isUserInteractionEnabled = true
    }
    
    func createDatePicker() {
        birthdatePicker.datePickerMode = .date
        birthdatePicker.locale = Locale(identifier: "tr")
        birthdatePicker.maximumDate = Date()
        print(txtBirthdate.text!.ToDate())
        birthdatePicker.setDate(txtBirthdate.text!.ToDate(), animated: true)
        
        txtBirthdate.inputView = birthdatePicker
        txtBirthdate.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(btnDonebirthdatePicker))
        txtBirthdate.keyboardToolbar.nextBarButton.setTarget(self, action: #selector(btnDonebirthdatePicker))
        txtBirthdate.keyboardToolbar.previousBarButton.setTarget(self, action: #selector(btnDonebirthdatePicker))
    }
    
    @objc func btnDonebirthdatePicker() {
        txtBirthdate.text = birthdatePicker.date.ToString()
    }
    
    func createCityPicker() {
        cityPicker.delegate = self
        cityPicker.dataSource = self
        
        txtCity.inputView = cityPicker
        txtCity.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(btnDoneCityPicker))
        txtCity.keyboardToolbar.nextBarButton.setTarget(self, action: #selector(btnDoneCityPicker))
        txtCity.keyboardToolbar.previousBarButton.setTarget(self, action: #selector(btnDoneCityPicker))
        
        // Insert Current City
        let currentCity = GlobalVariables.shared.jsonCity.filter() {
            city in
            return city.baslik == txtCity.text
        }
        if currentCity.count != 0 {
            if GlobalVariables.shared.jsonCity[0].baslik != txtCity.text {
                GlobalVariables.shared.jsonCity.insert(currentCity[0], at: 0)
            }
        }
    }
    
    @objc func btnDoneCityPicker() {
        txtCity.text = pickerView(cityPicker, titleForRow: cityPicker.selectedRow(inComponent: 0), forComponent: 0)
    }
    
    func createGenderPicker() {
        genderPicker.delegate = self
        genderPicker.dataSource = self
        
        txtGender.inputView = genderPicker
        txtGender.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(btnDoneGenderPicker))
        txtGender.keyboardToolbar.nextBarButton.setTarget(self, action: #selector(btnDoneGenderPicker))
        txtGender.keyboardToolbar.previousBarButton.setTarget(self, action: #selector(btnDoneGenderPicker))
        
        if txtGender.text == "Kadın" {
            genderPicker.selectRow(1, inComponent: 0, animated: false)
        }
    }
    
    @objc func btnDoneGenderPicker() {
        txtGender.text = pickerView(genderPicker, titleForRow: genderPicker.selectedRow(inComponent: 0), forComponent: 0)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    // MARK: Profile Image Change
    @objc func selectProfileImage() {
        editImage.isHidden = true
        GlobalFunctions.shared.animationShake(imgProfile)

        let picker = UIImagePickerController()
        picker.delegate = self // UINavigationControllerDelegate
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImage: UIImage?
        if let editImage = info["UIImagePickerControllerEditedImage"] {
            selectedImage = (editImage as! UIImage)
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] {
            selectedImage = originalImage as? UIImage
        }
        
        if let image = selectedImage {
            self.imgProfile.image = image
            isChangePhoto = true
        }
        
        dismiss(animated: true, completion: nil)
        GlobalFunctions.shared.animationShake(imgProfile)
    }
    
//    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//        print("cancel imagePicker")
//        dismiss(animated: true, completion: nil)
//    }
    
    // MARK:- PickerView Delegate & DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == cityPicker {
            return GlobalVariables.shared.jsonCity.count
        } else if pickerView == genderPicker {
            return 2
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == cityPicker {
            return GlobalVariables.shared.jsonCity[row].baslik
        } else if pickerView == genderPicker {
            return row == 0 ? "Erkek" : "Kadın"
        }
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    
}
