//
//  KisiselBilgilerimVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 23.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class KisiselBilgilerimVC: UIViewController {

    @IBOutlet weak var txtBirthdate: UITextField!
    let birthdatePicker = UIDatePicker()
    @IBOutlet weak var txtCity: UITextField!
    let cityPicker = UIPickerView()
    @IBOutlet weak var txtGender: UITextField!
    let genderPicker = UIPickerView()
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtLastname: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var editImage: UIView!
    
    @IBOutlet weak var txtTcNo: UITextField!
    
    @IBOutlet weak var profileView: UIView!
    var isChangePhoto = false
    
    @IBOutlet weak var HeaderTop: NSLayoutConstraint!
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11, *) {
            self.HeaderTop.constant = 60 + view.safeAreaInsets.top - self.navigationController!.navigationBar.frame.height - 20
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fillCustomer()
        createKeyboards()
        imgProfile.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectProfileImage)))
    }
    
    func fillCustomer() {
        if let result = GlobalVariables.shared.currentCustomer {
            self.txtCity.text = result.city!
            self.txtName.text = result.name
            self.txtLastname.text = result.lastname
            self.txtBirthdate.text = result.birthdate
            self.txtGender.text = result.gender
            self.txtEmail.text = result.email
            self.imgProfile.downloadedFrom(link: result.profil_foto)
            self.lblPhone.text = result.phone
            self.txtTcNo.text = result.tc
        }
    }
        
    @IBAction func btnKaydet(_ sender: Any) {
        if isValid() {
            updateCity()
            updateCustomer()
        }
    }
    
    func updateCity() {
        if let cityData = UserDefaults.standard.value(forKey: "jsonCity") as? Data {
            GlobalVariables.shared.jsonCity = try! PropertyListDecoder().decode([JsonCity.Response].self, from: cityData)
        } else {
            JsonCity.Todo.connect(completionHandler: { (result, error) in
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    

}
