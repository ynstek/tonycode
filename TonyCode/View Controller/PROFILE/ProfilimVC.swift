//
//  ProfilimVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 23.03.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class ProfilimVC: UIViewController {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblNameLastname: UILabel!
    
    @IBOutlet weak var HeaderTop: NSLayoutConstraint!
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11, *) {
            HeaderTop.constant = 60 + view.safeAreaInsets.top - self.navigationController!.navigationBar.frame.height - 20
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.presentTransparentNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let result = GlobalVariables.shared.currentCustomer {
            self.lblNameLastname.text = result.name! + " " + result.lastname!
            self.imgProfile.downloadedFrom(link: result.profil_foto)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.backButton()
    }
    
    
}
