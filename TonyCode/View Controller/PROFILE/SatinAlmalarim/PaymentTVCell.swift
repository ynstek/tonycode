//
//  PaymentTVCell.swift
//  TonyCode
//
//  Created by Yunus Tek on 5.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class PaymentTVCell: UITableViewCell {

    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var paymentDate: UILabel!
    @IBOutlet weak var price: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
