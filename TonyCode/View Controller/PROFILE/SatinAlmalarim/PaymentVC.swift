//
//  PaymentVC.swift
//  TonyCode
//
//  Created by Yunus Tek on 5.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class PaymentVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var HeaderTop: NSLayoutConstraint!
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11, *) {
            HeaderTop.constant = 60 + view.safeAreaInsets.top - self.navigationController!.navigationBar.frame.height - 20
        }
    }
    
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshControlData), for: .valueChanged)
        return refreshControl
    }()
    
    @objc func refreshControlData() {
        getPayments(false)
        refreshControl.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getPayments()
        self.tableView.insertSubview(self.refreshControl, at: 0)
    }

    func getPayments(_ activity: Bool = true) {
        if Reachability.isConnectedToNetwork() {
            if activity {
                self.activityOpen()
            }
            JsonPayment.Todo.connect(completionHandler: { (result, error) in
                self.activityClose()
                if result != nil {
                    DispatchQueue.main.async() {
                        GlobalVariables.shared.jsonPayment = result!
                        self.tableView.reloadData()
                    }
                }
            })
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = GlobalVariables.shared.jsonPayment.count
        
        tableView.isHidden = count == 0 ? true : false // For Bosview
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PaymentTVCell
        let data = GlobalVariables.shared.jsonPayment[indexPath.row]
        
        cell.time.text = data.time
        cell.paymentDate.text = data.paymentDate!.ToDate().ToString()
        cell.price.text = data.price! + " TL"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    

}
